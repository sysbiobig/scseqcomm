% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/intercellular.R
\name{compute_score_S_inter}
\alias{compute_score_S_inter}
\title{Function that takes as input the lists of S_l/r scores of each ligand and receptors of the ligand-expressing cluster and the receptor-expressing cluster and a list of ligands-receptors pair to compute the activation score of each ligand-receptor pair as the min value between the ligand and receptor score.}
\usage{
compute_score_S_inter(
  scores_cluster_l,
  scores_cluster_r,
  LR_pairs,
  cell_cluster,
  exprMatr,
  LR_subunits = FALSE,
  method = c("geometric.mean", "min"),
  exprMatr_type = "matrix",
  inter_scores = "scSeqComm"
)
}
\arguments{
\item{scores_cluster_l}{A named list of 2 elements: the list of ligand scores and receptor scores of the ligand-expressing cluster.}

\item{scores_cluster_r}{A named list of 2 elements: the list of ligand scores and receptor scores of the receptor-expressing cluster}

\item{LR_pairs}{A data.frame of ligands and receptors forming a ligand-receptor pairs; it must contain ligands and receptors for which a S_l/r score is computed in the 2 clusters}

\item{cell_cluster}{Named list of cell clusters. Each element of the list is an array of column names or column indices.}

\item{exprMatr}{Named processed count matrix (genes on rows, cells on columns).}

\item{LR_subunits}{Logical value indicating whether the used Ligand-Receptor database contains ligand/receptor multi-subunits}

\item{method}{A character string specifying the method that should be used to aggregate score in case of multisubunit ligands or receptor (default is \code{geometric.mean})}

\item{exprMatr_type}{A character vector specifying the object type of /code{exprMatr}; compatible types are "matrix","dgCMatrix" or "big.matrix". Default: "matrix".}

\item{inter_scores}{Intercellular signaling scoring schemes to be computed (default "scSeqComm"). Valid intercellular scoring schemes are c("scSeqComm", "Regularized_Product_score", "Zhou_score", "Skelly_score","scDiffCom_score", "NATMI_mean_expression", "NATMI_specificity", "CellChat_score").}
}
\value{
A data.frame containing information on ligand-receptor pair, the cluster couple, the related ligand/receptor S score and the activation score of the pair. Additional columns contain information about alternative implemented scoring schemes.
}
\description{
The function computes the activation score of each ligand-receptor pair for a given cell cluster pair as the min value between the ligand and receptor score of, respectively, the ligand-expressing cluster and the receptor-expressing cluster.
The function implements also alternative scoring schemes such as the regularized product score (Cabello-Aguilar et al. (2020)) and the product of ligand and receptor mean expression levels.
The cluster lists of scores must be named by its cluster name.
The ligand-receptor pairs must contain ligands and receptors for which the S score is computed (i.e. a subset of ligand or receptor score names).
}
\references{
Simon Cabello-Aguilar, Melissa Alame, Fabien Kon-Sun-Tack, Caroline Fau, Matthieu Lacroix, Jacques Colinge, SingleCellSignalR: inference of intercellular networks from single-cell transcriptomics, Nucleic Acids Research, Volume 48, Issue 10, 04 June 2020, Page e55, https://doi.org/10.1093/nar/gkaa183
}
