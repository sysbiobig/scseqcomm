## ----setup, include=FALSE-----------------------------------------------------------------------------------------------------------------------------------------------------
knitr::opts_chunk$set(echo = TRUE)

## ----echo=TRUE, error=F, results='hide', message=FALSE------------------------------------------------------------------------------------------------------------------------
library(scSeqComm)

########## Load scRNA-seq data ##########
# load example data (gene expression matrix and cell groups) from Tirosh et al. (2016)
data(Example_data_GSE72056)
gene_expr_matrix <- Example_data_GSE72056$GSE72056_log2tpm_filtered
cell_metadata <- Example_data_GSE72056$GSE72056_cellmetadata


## ----echo=TRUE, error=F-------------------------------------------------------------------------------------------------------------------------------------------------------
########## Ligand-receptor pairs ##########
# list available ligand-receptor pairs databases included in scSeqComm
?available_LR_pairs()
available_LR_pairs(species = "human")

# let's use the ligand-receptor pairs from Kumar et al. (2018)
?LR_pairs_Kumar_2018
LR_db <- LR_pairs_Kumar_2018

########## Transcriptional regulatory networks ##########
# list available transcriptional regulatory networks databases included in scSeqComm
?available_TF_TG()
available_TF_TG(species = "human")
# let's use the transcriptional regulatory networks from TRRUST v2 [Han et al. (2018)], 
# HTRIdb [Bovolenta et al. (2012)] and RegNetwork ("High confidence" entries) [Liu et al. (2015)]
?TF_TG_TRRUSTv2_HTRIdb_RegNetwork_High
TF_TG_db <- TF_TG_TRRUSTv2_HTRIdb_RegNetwork_High

########## Receptor-Transcription factor a-priori association  ##########
# list available receptor-transcription factor a-priori associations included in scSeqComm
?available_TF_PPR()
available_TF_PPR(species = "human")
# let's use the R-TF a-priori association obtained by KEGG signaling networks
?TF_PPR_KEGG_human
TF_PPR <- TF_PPR_KEGG_human


## ----echo=TRUE, error=F,results='hide', message=FALSE-------------------------------------------------------------------------------------------------------------------------
########## Cellular communication analysis ##########

# Define the number of cores for parallel execution 
# (more cores reduce execution time at the cost of higher RAM usage)
num_cores <- 2

##### Identify and quantify intercellular and intracellular signaling
comm_res <- scSeqComm_analyze(gene_expr = gene_expr_matrix,
                              cell_metadata = cell_metadata,
                              LR_pairs_DB = LR_db,
                              TF_reg_DB = TF_TG_db,
                              R_TF_association = TF_PPR,
                              N_cores = num_cores)

inter_intra_scores <- comm_res$comm_results

## ----echo=FALSE, results='asis'-----------------------------------------------------------------------------------------------------------------------------------------------
tmp_res <- rbind(
  c("CCR5" ,"CCL5", "T_cell", "Macrophage", "0.985", "0.823", "Chemokine signaling pathway", "TNFSF10,IFI27,CD44,..."),
  c("CCR5" ,"CCL5", "T_cell", "Macrophage", "0.985", "0.654", "Human immunodeficiency virus 1 infection", "MMP2,SOX7,CLU,EZR,..."),
  c("CCR5" ,"CCL5", "B_cell", "Macrophage", "0.782", "0.823", "Chemokine signaling pathway", "TNFSF10,CASP10,CD44,..."),
  c("CCR5" ,"CCL5", "B_cell", "Macrophage", "0.782", "0.654", "Human immunodeficiency virus 1 infection", "MMP2,SOX7,CLU,EZR,..."),
  c("CCR5" ,"CCL5", "Macrophage", "B_cell", "0.357", "0.024", "Chemokine signaling pathway", "FBXO32,TNFSF10,VEGFA,..."),
  c("CCR5" ,"CCL5", "Macrophage", "B_cell", "0.357", "0.001", "Human immunodeficiency virus 1 infection", "GSTP1,FOS,MAPK1,..."),
  c("A2M" ,"LRP1", "T_cell", "B_cell", "0.324", "NA", "NA", "NA"),
  c("..." ,"...", "...", "...", "...", "...", "...", "...")
)
colnames(tmp_res) <- c("ligand" ,"receptor", "cluster_L", "cluster_R", "S_inter", "S_intra", "pathway", "list_genes")
knitr::kable(tmp_res)

## ----echo=TRUE, error=F-------------------------------------------------------------------------------------------------------------------------------------------------------
#summarise S_intra score for each LR pair and cell cluster couple as max S_intra
inter_max_intra_scores <- scSeqComm_summaryze_S_intra(inter_intra_scores)

##### Ongoing cellular communication among cell types #####

## select communication with "strong" intercellular evidence (e.g. > 0.8)
sinter_TH <- 0.8
selected_comm <- scSeqComm_select(inter_max_intra_scores, 
                                  S_inter = sinter_TH)

## ----echo=TRUE, error=F, fig.width=7, fig.height=6, out.align = 'center'------------------------------------------------------------------------------------------------------
# interactive chord diagram - Figure A)
scSeqComm_chorddiag_cardinality(data = selected_comm)
# heatmap - Figure B)
scSeqComm_heatmap_cardinality(data = selected_comm, 
                              title = "Ongoing cellular communication (intercellular evidence only)")

## ----echo=TRUE, error=F,fig.width=7, fig.height=6 , fig.align = 'center'------------------------------------------------------------------------------------------------------
# select communication with "strong" intercellular AND intracellular evidence (e.g. s_inter > 0.8 and s_intra > 0.8)
sintra_TH <- 0.8
selected_comm <- scSeqComm_select(inter_max_intra_scores, 
                                  S_inter = sinter_TH, 
                                  S_intra = sintra_TH)
# interactive chord diagram - Figure C)
scSeqComm_chorddiag_cardinality(data = selected_comm)
# heatmap - Figure D)
scSeqComm_heatmap_cardinality(data = selected_comm, 
                              title = "Ongoing cellular communication (inter- and intra-cellular evidence)")

## ----echo=TRUE, error=F, fig.width=7, fig.height=6, , fig.align = 'center'----------------------------------------------------------------------------------------------------
## select communication with "medium" intercellular evidence (e.g. > 0.5)
sinter_TH <- 0.5
selected_comm <- scSeqComm_select(inter_max_intra_scores, 
                                  S_inter = sinter_TH)
# interactive chord diagram - Figure A)
scSeqComm_chorddiag_cardinality(data = selected_comm)
# heatmap - Figure B)
scSeqComm_heatmap_cardinality(data = selected_comm, title = "Ongoing cellular communication (intercellular evidence only)")

# select communication with "medium" intercellular AND intracellular evidence (e.g. s_inter > 0.5 and s_intra > 0.5)
sintra_TH <- 0.5
selected_comm <- scSeqComm_select(inter_max_intra_scores, 
                                  S_inter = sinter_TH, 
                                  S_intra = sintra_TH)
# interactive chord diagram - Figure C)
scSeqComm_chorddiag_cardinality(data = selected_comm)
# heatmap - Figure D)
scSeqComm_heatmap_cardinality(data = selected_comm, title = "Ongoing cellular communication (inter- and intra-cellular evidence)")

## ----echo=TRUE, error=F-------------------------------------------------------------------------------------------------------------------------------------------------------
### chemokine pairs
chemokine_lig <- c("CX3CL1","CXCL2","CXCL3","CXCL9","CXCL10","CXCL12","CXCL13","CXCL16",
                   "CCL14","CCL16","CCL2","CCL21","CCL3","CCL3L3","CCL4","CCL5")
chemokine_rec <- c("CX3CR1", "CXCR2", "CXCR3", "CXCR4", "CXCR5", "CXCR6",
                   "CCR1","CCR2","CCR5","CCR7","CCRL2")

chemokine_comm <- scSeqComm_select(inter_max_intra_scores, operator = "OR",
                                   ligand = chemokine_lig, receptor = chemokine_rec)

## ----echo=TRUE, error=F, fig.width=10, fig.height=10, fig.align = 'center'----------------------------------------------------------------------------------------------------
# plot group by receptor and receptor cluster - Figure S5
scSeqComm_plot_scores(data = chemokine_comm,
              title = "Ligand-receptor pairs involving chemokines - Intercellular and intracellular signaling analysis")

## ----echo=TRUE, error=F, fig.width=10, fig.height=12, fig.align = 'center'----------------------------------------------------------------------------------------------------
# plot group by ligand and ligand cluster
scSeqComm_plot_scores(data = chemokine_comm, 
              title = "Ligand-receptor pairs involving chemokines - Intercellular and intracellular signaling analysis",
              facet_grid_x = "cluster_L", facet_grid_y = "ligand")

## ----echo=TRUE, error=F, fig.align='center', fig.height=5, fig.width=6--------------------------------------------------------------------------------------------------------
## specific cell cluster pairs

# Macrophage --> CAF - Figure A)
# first select entries related to the given cell clusters pair
specific_cluster_pair_comm <- scSeqComm_select(chemokine_comm, 
                                               cluster_L = "Macrophage", cluster_R = "CAF")
# then plot the selected entries
scSeqComm_plot_cluster_pair (data = specific_cluster_pair_comm, 
                             title = "Chemokine - Macrophage --> CAF")

# CAF --> Macrophage - Figure B)
# alternative: specify the given cell clusters pair as input parameter
scSeqComm_plot_cluster_pair (data = chemokine_comm, 
                             selected_cluster_pair = "CAF --> Macrophage", 
                             title = "Chemokine - CAF --> Macrophage")

## ----echo=TRUE, error=F, fig.align='center', fig.height=4, fig.width=5--------------------------------------------------------------------------------------------------------
## specific ligand-receptor pairs

pair <- "CCL2 - CCR5" # Figure A)
# first select entries related to the given LR pair
specific_LR_pair_comm <- scSeqComm_select(chemokine_comm, LR_pair = pair) 
# then plot the selected entries
scSeqComm_plot_LR_pair(data = specific_LR_pair_comm, title = pair)

pair <- "CXCL12 - ITGB1" # Figure B)
# alternative: specify the given LR pair as input parameter
scSeqComm_plot_LR_pair(data = chemokine_comm, title = pair, selected_LR_pair = pair)

## ----warning=F, error=F,results='hide', message=FALSE-------------------------------------------------------------------------------------------------------------------------
##### CAF specific cellular communication #####

## select cellular communication
# - with "strong" intercellular AND intracellular evidence (e.g. s_inter > 0.8 and s_intra > 0.8)
# - specific of CAF cells (i.e. they do not show a high evidence of being involved in other groups of cells)

sinter_TH <- 0.8
sintra_TH <- 0.8
all_cluster <- unique(inter_intra_scores$cluster_R)
all_pair <- unique(inter_intra_scores$LR_pair)
selected_cluster <- "CAF"

# ligand-receptor pairs with strong intercellular AND intracellular evidence in the other cell clusters
non_specific_pair <- unique(scSeqComm_select(inter_intra_scores, 
                                              cluster_R = setdiff(all_cluster,selected_cluster),
                                              S_inter = sinter_TH, S_intra = sintra_TH, 
                                              NA_included = FALSE, 
                                              output_field = "LR_pair"))

# ligand-receptor pairs with strong intercellular AND intracellular evidence ONLY in CAF cells
CAF_specific_comm <- scSeqComm_select(data = inter_intra_scores,
                                      cluster_R = selected_cluster,
                                      LR_pair = setdiff(all_pair, non_specific_pair),
                                      S_inter = sinter_TH, S_intra = sintra_TH,
                                      NA_included = FALSE)
CAF_specific_pair <- unique(CAF_specific_comm$LR_pair)




## ----echo=TRUE, error=F-------------------------------------------------------------------------------------------------------------------------------------------------------
# 29 ligand-receptor pairs specific of cluster "CAF"
cat(as.character(CAF_specific_pair), sep = ", ","\n") 

## ----warning=F, error=F, results='hide', message=FALSE------------------------------------------------------------------------------------------------------------------------
## functionally characterize the cellular responses associated to the seleceted ligand-receptor pairs

# gene universe is all the target genes in dataset
geneUniverse <- unique(unlist(comm_res$TF_reg_DB_scrnaseq))

# GO analysis of CAF specific communication
enrich_res_CAF_general_specificpair <- scSeqComm_GO_analysis(results_signaling = CAF_specific_comm,
                                                             geneUniverse = geneUniverse,
                                                              method = "general")


## ----warning=F, error=F, eval=FALSE,include=TRUE------------------------------------------------------------------------------------------------------------------------------
# head(enrich_res_CAF_general_specificpair, n = 4)

## ----echo=FALSE, results='asis'-----------------------------------------------------------------------------------------------------------------------------------------------
tmp_GO_global <- rbind(
  c("GO:0045766" ,"positive regulation of angiogenesis", "107", "29", "6.50", "7.8e-09", "0.000106938"),
  c("GO:0030198" ,"extracellular matrix organization", "204", "50", "12.40", "2.1e-08", "0.000287910"),
  c("GO:0002480" ,"antigen processing and presentation of exogenous peptide antigen via MHC class I, TAP-independent", "8", "7", "0.49", "2.2e-08", "0.000301620"),
  c("GO:00051897" ,"positive regulation of protein kinase B signaling", "103", "23", "6.26", "3.3e-08", "0.000452430"),
  c("..." , "...", "...", "...", "...", "...", "...")
)
colnames(tmp_GO_global) <- c("GO.ID" ,"Term", "Annotated", "Significant", "Expected", "   pval   ", "pval.adjusted")
knitr::kable(tmp_GO_global)

## ----warning=F, error=F, eval=FALSE, include=TRUE-----------------------------------------------------------------------------------------------------------------------------
# 
# # KEGG pathways associated to CAF specific communication (for visualization purposes)
# KEGG_CAF_specific <- scSeqComm_select(data = CAF_specific_comm,
#                                       output_field = "pathway")
# 
# # select results associated to CAF specific cellular communication (for visualization purposes)
# CAF_specific_comm_plot <- scSeqComm_select(inter_intra_scores,
#                                       cluster_R = "CAF",
#                                       LR_pair = CAF_specific_pair,
#                                       pathway = KEGG_CAF_specific)
# 
# # plot (GO global)
# scSeqComm_plot_scores_pathway(data = CAF_specific_comm_plot,
#                   title = "CAF intercellular, intracellular and functional analysis",
#                   annotation_GO = enrich_res_CAF_general_specificpair, cutoff = 0.05, topGO = 20)

## ----out.width = "100%", echo = FALSE, out.align = "right"--------------------------------------------------------------------------------------------------------------------
knitr::include_graphics("images/Figure_7_vignette.jpg")

## ----warning=F, error=F, results='hide', message=FALSE------------------------------------------------------------------------------------------------------------------------
# GO analysis of CAF specific communication (stratified by receptors)
enrich_res_CAF_specific_specificpair <- scSeqComm_GO_analysis(results_signaling = CAF_specific_comm,
                                            geneUniverse = geneUniverse,
                                            method = "specific")

## ----echo=TRUE, error=F-------------------------------------------------------------------------------------------------------------------------------------------------------
names(enrich_res_CAF_specific_specificpair)

## ----warning=F, error=F, eval=FALSE, include=TRUE-----------------------------------------------------------------------------------------------------------------------------
# head(enrich_res_CAF_specific_specificpair[["CD44"]], n = 3) # potential functional responses triggered by receptor "CD44"

## ----echo=FALSE, results='asis'-----------------------------------------------------------------------------------------------------------------------------------------------
tmp_GO_CD44 <- rbind(
  c("GO:0045766 ", "positive regulation of angiogenesis", "107", "27", "5.47", "2.6e-09", "3.5646e-05"),
  c("GO:0002486", "antigen processing and presentation of endogenous peptide antigen via MHC class I via ER pathway, TAP-independent", "6", "6", "0.31", "1.7e-08", "2.3307e-04"),
  c("GO:0001666", "response to hypoxia", "231", "36", "11.80", "9.5e-08", "1.30245e-03"),
  c("...", "...", "...", "...", "...", "...", "...")
)
colnames(tmp_GO_CD44) <- c("GO.ID" ,"Term", "Annotated", "Significant", "Expected", "   pval   ", "pval.adjusted")
knitr::kable(tmp_GO_CD44)

## ----warning=F, error=F, eval=FALSE, include=TRUE-----------------------------------------------------------------------------------------------------------------------------
# # plot (GO stratified by receptor)
# scSeqComm_plot_scores_pathway(data = CAF_specific_comm_plot,
#                     title = "CAF intercellular, intracellular and functional analysis",
#                     annotation_GO = enrich_res_CAF_specific_specificpair, cutoff = 0.05, topGO = 20)

## ----out.width = "100%", echo = FALSE, out.align = "right"--------------------------------------------------------------------------------------------------------------------
knitr::include_graphics("images/Figure_7_1_Vignette.jpg")

