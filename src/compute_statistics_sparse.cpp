#include <Rcpp.h>
#include "sparse.h"

//[[Rcpp::export]]
Rcpp::NumericVector rowSumsColIndexNativeSparse(Rcpp::S4& M, Rcpp::NumericVector colInd) {
  // MySparseMatrix B = MySparseMatrix(A);
  return MySparseMatrix(M).rowSumsColIndex(colInd);
}

//[[Rcpp::export]]
Rcpp::NumericVector rowMeansColIndexSparse(Rcpp::S4& M, Rcpp::NumericVector colInd) {
  return MySparseMatrix(M).rowMeansColIndex(colInd);
}

//[[Rcpp::export]]
Rcpp::List BigBootstrapSparse(Rcpp::S4& M,
                              Rcpp::NumericVector colIndex,
                              Rcpp::NumericVector H0_genes,
                              Rcpp::NumericVector ligands,
                              Rcpp::NumericVector receptors){
  return MySparseMatrix(M).BigBootstrap(colIndex, H0_genes, ligands, receptors);
}

//[[Rcpp::export]]
double MeanSparseColIndex(Rcpp::S4& M, Rcpp::NumericVector colIndex){
  return MySparseMatrix(M).MeanSparseColIndex(colIndex);
}

//[[Rcpp::export]]
double MeanSparse(Rcpp::S4& M){
  return MySparseMatrix(M).MeanSparse();
}

//[[Rcpp::export]]
Rcpp::NumericVector rowMeansRowColIndexSparse(Rcpp::S4& M,
                                              Rcpp::NumericVector rowInd,
                                              Rcpp::NumericVector colInd) {
  return MySparseMatrix(M).rowMeansRowColIndex(rowInd, colInd);
}

//[[Rcpp::export]]
Rcpp::List BigBootstrapSparseParallel(Rcpp::S4& M,
                                      Rcpp::NumericVector colIndex,
                                      Rcpp::NumericVector H0_genes,
                                      Rcpp::NumericVector ligands,
                                      Rcpp::NumericVector receptors){
  return MySparseMatrix(M).BigBootstrapParallel(colIndex, H0_genes, ligands, receptors);
}

//[[Rcpp::export]]
Rcpp::NumericVector sparseCountElementByRow_ColInd(Rcpp::S4& M,
                                                   Rcpp::NumericVector colIndex,
                                                   double count_thr) {
  return MySparseMatrix(M).sparseCountElementByRow_ColInd(colIndex, count_thr);
}

//[[Rcpp::export]]
Rcpp::NumericVector sparseCountElementByRow_RowInd_ColInd(Rcpp::S4& M,
                                                          Rcpp::NumericVector rowIndex,
                                                   Rcpp::NumericVector colIndex,
                                                   double count_thr) {
  return MySparseMatrix(M).sparseCountElementByRow_RowInd_ColInd(rowIndex, colIndex, count_thr);
}

// //[[Rcpp::export]]
// Rcpp::List sparseWilc(Rcpp::S4& M,
//                       Rcpp::NumericVector rowIndex,
//                       Rcpp::NumericVector col_id,
//                       Rcpp::NumericVector ref_col_id,
//                       bool verbose = 0){
//   return MySparseMatrix(M).sparseWilc(rowIndex, col_id, ref_col_id);
// }

//[[Rcpp::export]]
std::vector<std::vector<double>> extractCol(Rcpp::S4& M,
                                            Rcpp::NumericVector colIndex) {
  return MySparseMatrix(M).extractCol(colIndex);
}


// [[Rcpp::export]]
void fromSparsetoBigMatrix(Rcpp::S4& M,
                           SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  //  cout << xpMat->matrix_type() << endl;

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  // cout << xpMat->matrix_type() << "\n";

  switch(xpMat->matrix_type()) {
  case 2:
    return MySparseMatrix(M).fromSparsetoBigMatrix(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return MySparseMatrix(M).fromSparsetoBigMatrix(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return MySparseMatrix(M).fromSparsetoBigMatrix(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return MySparseMatrix(M).fromSparsetoBigMatrix(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}
