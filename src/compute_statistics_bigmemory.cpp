#include <Rcpp.h>
#include "sparse.h"
// [[Rcpp::plugins(cpp11)]]
// [[Rcpp::depends(BH, bigmemory)]]
#include <bigmemory/MatrixAccessor.hpp>

#include <numeric>

#include <math.h>
using u_int = unsigned int;

using namespace Rcpp;

/*********************** New_BigBootstrap ***********************/

// Logic for New_BigBootstrap.
template <typename T>
List BigBootstrap(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                      NumericVector colIndex,
                      NumericVector H0_genes,
                      NumericVector ligands, NumericVector receptors) {

  // remove 1 to all input array indices

  const int N_COL = colIndex.length();
  u_int colIndexC[N_COL];
  for(int i = 0; i < N_COL; ++i){
    colIndexC[i] = colIndex[i] - 1;
  }

  const int N_GENES = H0_genes.length();
  u_int geneIndexC[N_GENES];
  for(int i = 0; i < N_GENES; ++i){
    geneIndexC[i] = H0_genes[i] - 1;
  }

  const int N_LIG = ligands.length();
  u_int ligIndexC[N_LIG];
  for(int i = 0; i < N_LIG; ++i){
    ligIndexC[i] = ligands[i] - 1;
  }

  const int N_REC = receptors.length();
  u_int recIndexC[N_REC];
  for(int i = 0; i < N_REC; ++i){
    recIndexC[i] = receptors[i] - 1;
  }

  /**
   * Compute ligands and receptors average expression level on the given bootstrap matrix
   */

  // create empty, i.e. initialized with 0, array for ligands and receptors average expression levels
  NumericVector ligands_avg(N_LIG, 0.0);
  NumericVector receptors_avg(N_REC, 0.0);

  int tmp_j = 0;
  int tmp_i = 0;
  int ii_l = 0;
  int ii_r = 0;
  int ii_g = 0;


  // for each column in the "bootstrap"
  /*
   for (int jj = 0; jj < N_COL; ++jj) {
   // update the cumulative sum of the ligands with the element in the current column
   for (ii_l = 0; ii_l < N_LIG; ++ii_l) { // for each ligand
   ligands_avg[ii_l] += mat[ colIndexC[jj] ][ ligIndexC[ii_l] ];
   }
   // update the cumulative sum of the receptors with the element in the current column
   for (ii_r = 0; ii_r < N_REC; ++ii_r) { // for each receptor
   receptors_avg[ii_r] += mat[ colIndexC[jj] ][ recIndexC[ii_r] ];
   }
   }
   */
  const int tmp_NCOL0 = N_COL - N_COL%4;
  int j1 = 0;
  int j2 = 0;
  int j3 = 0;
  int j4 = 0;
  for (int jj = 0; jj < tmp_NCOL0; jj = jj+4) {
    // update the cumulative sum of the ligands with the element in the current column
    j1 = colIndexC[jj];
    j2 = colIndexC[jj+1];
    j3 = colIndexC[jj+2];
    j4 = colIndexC[jj+3];


    for (ii_l = 0; ii_l < N_LIG; ++ii_l) { // for each ligand
      ligands_avg[ii_l] += mat[ j1 ][ ligIndexC[ii_l] ] +
        mat[ j2 ][ ligIndexC[ii_l] ] +
        mat[ j3 ][ ligIndexC[ii_l] ] +
        mat[ j4 ][ ligIndexC[ii_l] ] ;
    }
    // update the cumulative sum of the receptors with the element in the current column
    for (ii_r = 0; ii_r < N_REC; ++ii_r) { // for each receptor
      receptors_avg[ii_r] += mat[ j1 ][ recIndexC[ii_r] ] +
        mat[ j2 ][ recIndexC[ii_r] ] +
        mat[ j3 ][ recIndexC[ii_r] ] +
        mat[ j4 ][ recIndexC[ii_r] ] ;
    }
  }
  for (int jj = tmp_NCOL0; jj < N_COL; ++jj) {
    // update the cumulative sum of the ligands with the element in the current column
    for (ii_l = 0; ii_l < N_LIG; ++ii_l) { // for each ligand
      ligands_avg[ii_l] += mat[ colIndexC[jj] ][ ligIndexC[ii_l] ];
    }
    // update the cumulative sum of the receptors with the element in the current column
    for (ii_r = 0; ii_r < N_REC; ++ii_r) { // for each receptor
      receptors_avg[ii_r] += mat[ colIndexC[jj] ][ recIndexC[ii_r] ];
    }
  }



  // compute the mean, i.e. divide the cumulative sum by the number of columns in the "bootstrap"
  ligands_avg = ligands_avg / (N_COL*1.0);
  receptors_avg = receptors_avg / (N_COL*1.0);






  /**
   * Compute the MEAN of the given bootstrap matrix
   */

  double H0_mean = 0.0;


  // create a empty, i.e. initialized with zero, col sums vector
  NumericVector colSums(N_COL, 0.0);

  // for each column in the "bootstrap"
  /*
   for (int jj = 0; jj < N_COL; ++jj) {
   tmp_j = colIndexC[jj];
   // update the cumulative sum of that columns
   for (ii_g = 0; ii_g < N_GENES; ++ii_g) { // for each gene
   colSums[jj] += mat[ tmp_j ][ geneIndexC[ii_g] ];
   }
   }*/


  const int tmp_NCOL = N_COL - N_COL%4;
  int tmp_j1, tmp_j2, tmp_j3, tmp_j4;
  for (int jj = 0; jj < tmp_NCOL; jj = jj+4) {
    tmp_j1 = colIndexC[jj];
    tmp_j2 = colIndexC[jj+1];
    tmp_j3 = colIndexC[jj+2];
    tmp_j4 = colIndexC[jj+3];
    for (ii_g = 0; ii_g < N_GENES; ++ii_g) {
      colSums[jj]   += mat[ tmp_j1 ][ geneIndexC[ii_g] ];
      colSums[jj+1] += mat[ tmp_j2 ][ geneIndexC[ii_g] ];
      colSums[jj+2] += mat[ tmp_j3 ][ geneIndexC[ii_g] ];
      colSums[jj+3] += mat[ tmp_j4 ][ geneIndexC[ii_g] ];
    }
  }
  for (int jj = tmp_NCOL; jj < N_COL; ++jj) {
    tmp_j = colIndexC[jj];
    for (ii_g = 0; ii_g < N_GENES; ++ii_g) {
      colSums[jj] += mat[ tmp_j ][ geneIndexC[ii_g] ];
    }
  }


  // compute the average of each column, i.e. cumulative sum of each column divide by the number of rows
  colSums = colSums / N_GENES;
  H0_mean = ( std::accumulate(colSums.begin(), colSums.end(), 0.0) ) / N_COL;


  /**
   * Compute the SD of the given bootstrap matrix
   */

  double H0_sd = 0.0;



  const double K = H0_mean; // shift parameter
  const double n = N_GENES * N_COL; // total number of elements
  double ex = 0.0;
  double ex2 = 0.0;

  double tmp_mat_entry = 0.0;

  /*
   for (int jj = 0; jj < N_COL; ++jj) { // for each column
   for (ii_g = 0; ii_g < N_GENES; ++ii_g) { // for each gene
   tmp_mat_entry = mat[ colIndexC[jj] ][ geneIndexC[ii_g] ] - K;
   ex += tmp_mat_entry;
   ex2 += tmp_mat_entry*tmp_mat_entry;
   }
   }
   */


  const int tmp_NCOL2 = N_COL - N_COL%8;
  double tmp_mat_entry1 = 0.0;
  double tmp_mat_entry2 = 0.0;
  double tmp_mat_entry3 = 0.0;
  double tmp_mat_entry4 = 0.0;
  double tmp_mat_entry5 = 0.0;
  double tmp_mat_entry6 = 0.0;
  double tmp_mat_entry7 = 0.0;
  double tmp_mat_entry8 = 0.0;
  for (int jj = 0; jj < tmp_NCOL2; jj = jj+8) { // for each column
    for (ii_g = 0; ii_g < N_GENES; ii_g++) { // for each gene
      tmp_mat_entry1 = mat[ colIndexC[jj  ] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry2 = mat[ colIndexC[jj+1] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry3 = mat[ colIndexC[jj+2] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry4 = mat[ colIndexC[jj+3] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry5 = mat[ colIndexC[jj+4] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry6 = mat[ colIndexC[jj+5] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry7 = mat[ colIndexC[jj+6] ][ geneIndexC[ii_g] ] - K;
      tmp_mat_entry8 = mat[ colIndexC[jj+7] ][ geneIndexC[ii_g] ] - K;

      ex += tmp_mat_entry1 + tmp_mat_entry2 + tmp_mat_entry3 + tmp_mat_entry4 +
        tmp_mat_entry5 + tmp_mat_entry6 + tmp_mat_entry7 + tmp_mat_entry8 ;

      ex2 += (tmp_mat_entry1*tmp_mat_entry1) + (tmp_mat_entry2*tmp_mat_entry2) +
        (tmp_mat_entry3*tmp_mat_entry3) + (tmp_mat_entry4*tmp_mat_entry4) +
        (tmp_mat_entry5*tmp_mat_entry5) + (tmp_mat_entry6*tmp_mat_entry6) +
        (tmp_mat_entry7*tmp_mat_entry7) + (tmp_mat_entry8*tmp_mat_entry8) ;
    }
  }
  for (int jj = tmp_NCOL2; jj < N_COL; ++jj) { // for each column
    for (ii_g = 0; ii_g < N_GENES; ++ii_g) { // for each gene
      tmp_mat_entry = mat[ colIndexC[jj] ][ geneIndexC[ii_g] ] - K;
      ex += tmp_mat_entry;
      ex2 += tmp_mat_entry*tmp_mat_entry;
    }
  }


  H0_sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );
  H0_sd = H0_sd / sqrt(N_COL*1.0);


  // create a List containing the main results
  List results = List::create(
    Named("lig_avg_expr") = ligands_avg,
    Named("rec_avg_expr") = receptors_avg,
    Named("H0_mean") = H0_mean,
    Named("H0_sd") = H0_sd
  );

  return results;
}

// // Logic for BigBootstrap.
// template <typename T>
// List BigBootstrap(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
//                            NumericVector colIndex,
//                            NumericVector H0_genes,
//                            NumericVector ligands, NumericVector receptors) {
//
//   /**
//    * Compute ligands and receptors average expression level on the given bootstrap matrix
//   */
//
//   // create empty, i.e. initialized with 0, array for ligands and receptors average expression levels
//   NumericVector ligands_avg(ligands.length(), 0.0);
//   NumericVector receptors_avg(receptors.length(), 0.0);
//
//   int tmp_j = 0;
//   int tmp_i = 0;
//
//   // for each column in the "bootstrap"
//   for (int jj = 0; jj < colIndex.length(); ++jj) {
//     tmp_j = colIndex[jj] - 1; // compute the index of that column in the "bootstrap". Index in R are 1-based, so remove 1.
//     // update the cumulative sum of the ligands with the element in the current column
//     for (int ii = 0; ii < ligands.length(); ++ii) { // for each ligand
//       tmp_i = ligands[ii] - 1; // compute the index of that ligand (row index). Index in R are 1-based, so remove 1.
//       ligands_avg[ii] += mat[tmp_j][tmp_i];
//     }
//
//     // update the cumulative sum of the receptors with the element in the current column
//     for (int ii = 0; ii < receptors.length(); ++ii) { // for each receptor
//       tmp_i = receptors[ii] - 1; // compute the index of that receptor (row index). Index in R are 1-based, so remove 1.
//       receptors_avg[ii] += mat[tmp_j][tmp_i];
//     }
//   }
//
//   // compute the mean, i.e. divide the cumulative sum by the number of columns in the "bootstrap"
//   ligands_avg = ligands_avg / colIndex.length();
//   receptors_avg = receptors_avg / colIndex.length();
//
//
//
//   /**
//    * Compute the MEAN of the given bootstrap matrix
//   */
//
//   double H0_mean = 0.0;
//
//
//   // create a empty, i.e. initialized with zero, col sums vector
//   // NumericVector colSums(colIndex.length(), 0.0);
//
//   // for each column in the "bootstrap"
//   for (int jj = 0; jj < colIndex.length(); ++jj) {
//     tmp_j = colIndex[jj] - 1; // compute the index of that column in the "bootstrap". Index in R are 1-based, so remove 1.
//
//     // update the cumulative sum of that columns
//     for (int ii = 0; ii < H0_genes.length(); ++ii) { // for each gene
//       tmp_i = H0_genes[ii] - 1; // Index in R are 1-based, so remove 1.
//
//       H0_mean += mat[tmp_j][tmp_i];
//       // colSums[jj] += mat[tmp_j][tmp_i];
//     }
//
//   }
//   // compute the average of each column, i.e. cumulative sum of each column divide by the number of rows
//   // colSums = colSums / H0_genes.length();
//   // H0_mean = ( std::accumulate(colSums.begin(), colSums.end(), 0.0) ) / colIndex.length();
//   H0_mean = H0_mean /(H0_genes.length() * colIndex.length());
//
//   /**
//    * Compute the SD of the given bootstrap matrix
//   */
//
//   double H0_sd = 0.0;
//
//   double K = H0_mean; // shift parameter
//   double n = H0_genes.length() * colIndex.length(); // total number of elements
//   double ex = 0.0;
//   double ex2 = 0.0;
//
//   double diff = 0;
//
//   for (int jj = 0; jj < colIndex.length(); ++jj) { // for each column
//     tmp_j = colIndex[jj] - 1; // Index in R are 1-based, so remove 1.
//     for (int ii = 0; ii < H0_genes.length(); ++ii) { // for each gene
//       tmp_i = H0_genes[ii] -1; // Index in R are 1-based, so remove 1.
//       diff = mat[tmp_j][tmp_i] - K;
//       ex += diff;
//       ex2 += (diff)*(diff);
//     }
//   }
//
//   H0_sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );
//   H0_sd = H0_sd / sqrt(colIndex.length()*1.0);
//
//   // create a List containing the main results
//   List results = List::create(
//     Named("lig_avg_expr") = ligands_avg,
//     Named("rec_avg_expr") = receptors_avg,
//     Named("H0_mean") = H0_mean,
//     Named("H0_sd") = H0_sd
//   );
//
//   return results;
// }




// Logic for BigBootstrap.
template <typename T>
List BigBootstrap_without_zeros(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                NumericVector colIndex,
                                NumericVector H0_genes,
                                NumericVector ligands, NumericVector receptors) {

  /**
   * Compute ligands and receptors average expression level on the given bootstrap matrix
   * CONSIDERING ONLY NOT ZERO ENTRIES
   */

  // create empty, i.e. initialized with 0, array for ligands and receptors average expression levels
  NumericVector ligands_avg(ligands.length(), 0.0);
  NumericVector receptors_avg(receptors.length(), 0.0);

  // create empty, i.e. initialized with 0, array for ligands and receptors number of not zero entries
  // this would contain the number of elements to use in the computation of the mean
  NumericVector ligands_not_zeros_entries(ligands.length(), 0);
  NumericVector receptors_not_zeros_entries(receptors.length(), 0);

  // for each column in the "bootstrap"
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1; // compute the index of that column in the "bootstrap". Index in R are 1-based, so remove 1.
    // update the cumulative sum of the ligands with the element in the current column
    for (int ii = 0; ii < ligands.length(); ii++) { // for each ligand
      int tmp_i = ligands[ii] - 1; // compute the index of that ligand (row index). Index in R are 1-based, so remove 1.
      if(mat[tmp_j][tmp_i] != 0){
        ligands_avg[ii] += mat[tmp_j][tmp_i];
        ligands_not_zeros_entries[ii]++;
      }
    }

    // update the cumulative sum of the receptors with the element in the current column
    for (int ii = 0; ii < receptors.length(); ii++) { // for each receptor
      int tmp_i = receptors[ii] - 1; // compute the index of that receptor (row index). Index in R are 1-based, so remove 1.
      if(mat[tmp_j][tmp_i] != 0){
        receptors_avg[ii] += mat[tmp_j][tmp_i];
        receptors_not_zeros_entries[ii]++;
      }
    }
  }

  // compute the mean, i.e. divide the cumulative sum by the number of columns in the "bootstrap"
  ligands_avg = ligands_avg / ligands_not_zeros_entries;
  receptors_avg = receptors_avg / receptors_not_zeros_entries;



  /**
   * Compute the MEAN of the given bootstrap matrix
  */

  double H0_mean = 0.0;

  // create a empty, i.e. initialized with zero, col sums vector
  NumericVector colSums(colIndex.length(), 0.0);

  // create a empty, i.e. initialized with zero, vector to compute the number of not zero entries in each column
  NumericVector col_not_zero_entries(colIndex.length(), 0.0);

  // for each column in the "bootstrap"
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1; // compute the index of that column in the "bootstrap". Index in R are 1-based, so remove 1.

    // update the cumulative sum of that columns
    for (int ii = 0; ii < H0_genes.length(); ii++) { // for each gene
      int tmp_i = H0_genes[ii] - 1; // Index in R are 1-based, so remove 1.
      if(mat[tmp_j][tmp_i] != 0){
        colSums[jj] += mat[tmp_j][tmp_i];
        col_not_zero_entries[jj]++;
      }

    }

  }
  // compute the average of each column, i.e. cumulative sum of each column divide by the number of rows
  colSums = colSums / col_not_zero_entries;
  H0_mean = ( std::accumulate(colSums.begin(), colSums.end(), 0.0) ) / colIndex.length();


  /**
   * Compute the SD of the given bootstrap matrix
   */

  double H0_sd = 0.0;

  double K = H0_mean; // shift parameter
  double n = std::accumulate(col_not_zero_entries.begin(), col_not_zero_entries.end(), 0.0); // total number of NOT ZERO elements
  double ex = 0.0;
  double ex2 = 0.0;

  for (int jj = 0; jj < colIndex.length(); jj++) { // for each column
    int tmp_j = colIndex[jj] - 1; // Index in R are 1-based, so remove 1.
    for (int ii = 0; ii < H0_genes.length(); ii++) { // for each gene
      int tmp_i = H0_genes[ii] -1; // Index in R are 1-based, so remove 1.
      if(mat[tmp_j][tmp_i] != 0){
        ex += mat[tmp_j][tmp_i] - K;
        ex2 += (mat[tmp_j][tmp_i] - K)*(mat[tmp_j][tmp_i] - K);
      }

    }
  }

  H0_sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );
  H0_sd = H0_sd / sqrt(colIndex.length()*1.0);

  // create a List containing the main results
  List results = List::create(
    Named("lig_avg_expr") = ligands_avg,
    Named("rec_avg_expr") = receptors_avg,
    Named("H0_mean") = H0_mean,
    Named("H0_sd") = H0_sd
  );

  return results;
}




/*********************** CountElementByRow ***********************/

template <typename T>
NumericVector BigCountElementByRow(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                  String operand, double val){

  // Create the vector we'll store the count of the elements by row.
  NumericVector countByRow(pMat->nrow(), 0.0);

  if(operand == "="){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[jj][ii] == val){
          countByRow[ii]++;
        }
      }
    }
  }// =

  if(operand == ">="){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[jj][ii] >= val){
          countByRow[ii]++;
        }
      }
    }
  }// >=

  if(operand == ">"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[jj][ii] > val){
          countByRow[ii]++;
        }
      }
    }
  }// >

  if(operand == "<="){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[jj][ii] <= val){
          countByRow[ii]++;
        }
      }
    }
  }// <=

  if(operand == "<"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[jj][ii] < val){
          countByRow[ii]++;
        }
      }
    }
  }// <

  return countByRow;
}


template <typename T>
NumericVector BigCountElementByRow_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                          String operand, double val,
                                          NumericVector colIndex){

  // Create the vector we'll store the count of the elements by row.
  NumericVector countByRow(pMat->nrow(), 0.0);

  if(operand == "="){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[colIndex[jj]-1][ii] == val){
          countByRow[ii]++;
        }
      }
    }
  }// =

  if(operand == ">="){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[colIndex[jj]-1][ii] >= val){
          countByRow[ii]++;
        }
      }
    }
  }// >=

  if(operand == ">"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[colIndex[jj]-1][ii] > val){
          countByRow[ii]++;
        }
      }
    }
  }// >

  if(operand == "<="){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[colIndex[jj]-1][ii] <= val){
          countByRow[ii]++;
        }
      }
    }
  }// <=

  if(operand == "<"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        if(mat[colIndex[jj]-1][ii] < val){
          countByRow[ii]++;
        }
      }
    }
  }// <

  return countByRow;
}

template <typename T>
NumericVector BigCountElementByRow_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                          String operand, double val,
                                          NumericVector rowIndex){

  // Create the vector we'll store the count of the elements by row.
  NumericVector countByRow(rowIndex.length(), 0.0);

  if(operand == "="){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[jj][i] == val){
          countByRow[ii]++;
        }
      }
    }
  }// =

  if(operand == ">="){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[jj][i] >= val){
          countByRow[ii]++;
        }
      }
    }
  }// >=

  if(operand == ">"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[jj][i] > val){
          countByRow[ii]++;
        }
      }
    }
  }// >

  if(operand == "<="){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[jj][i] <= val){
          countByRow[ii]++;
        }
      }
    }
  }// <=

  if(operand == "<"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[jj][i] < val){
          countByRow[ii]++;
        }
      }
    }
  }// <

  return countByRow;
}

template <typename T>
NumericVector BigCountElementByRow_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                                String operand, double val,
                                                NumericVector rowIndex,
                                                NumericVector colIndex){

  // Create the vector we'll store the count of the elements by row.
  NumericVector countByRow(rowIndex.length(), 0.0);

  if(operand == "="){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[colIndex[jj]-1][i] == val){
          countByRow[ii]++;
        }
      }
    }
  }// =

  if(operand == ">="){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[colIndex[jj]-1][i] >= val){
          countByRow[ii]++;
        }
      }
    }
  }// >=

  if(operand == ">"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[colIndex[jj]-1][i] > val){
          countByRow[ii]++;
        }
      }
    }
  }// >

  if(operand == "<="){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[colIndex[jj]-1][i] <= val){
          countByRow[ii]++;
        }
      }
    }
  }// <=

  if(operand == "<"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        if(mat[colIndex[jj]-1][i] < val){
          countByRow[ii]++;
        }
      }
    }
  }// <

  return countByRow;
}




/*********************** Operation ByRow ***********************/

template <typename T>
void BigOperationByRow(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                    String operand, NumericVector val){

  if(operand == "+"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[jj][ii] += val[ii];
      }
    }
  }// +

  if(operand == "-"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[jj][ii] -= val[ii];
      }
    }
  }// -

  if(operand == "/"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[jj][ii] /= val[ii];
      }
    }
  }// >

  if(operand == "*"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[jj][ii] *= val[ii];
      }
    }
  }// *

}


template <typename T>
void BigOperationByRow_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                            String operand, NumericVector val,
                                            NumericVector colIndex){

  // remove possible duplicates from column index
  sort( colIndex.begin(), colIndex.end() );
  colIndex.erase( unique( colIndex.begin(), colIndex.end() ), colIndex.end() );

  if(operand == "+"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[colIndex[jj]-1][ii] += val[ii];
      }
    }
  }// +

  if(operand == "-"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[colIndex[jj]-1][ii] -= val[ii];
      }
    }
  }// -

  if(operand == "/"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[colIndex[jj]-1][ii] /= val[ii];
      }
    }
  }// /

  if(operand == "*"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < pMat->nrow(); ii++) {
        mat[colIndex[jj]-1][ii] *= val[ii];
      }
    }
  }// *

}

template <typename T>
void BigOperationByRow_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                            String operand, NumericVector val,
                                            NumericVector rowIndex){

  // remove possible duplicates from row index
  sort( rowIndex.begin(), rowIndex.end() );
  rowIndex.erase( unique( rowIndex.begin(), rowIndex.end() ), rowIndex.end() );

  if(operand == "+"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[jj][i] += val[i];
      }
    }
  }// +

  if(operand == "-"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[jj][i] -= val[i];
      }
    }
  }// -

  if(operand == "/"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[jj][i] /= val[i];
      }
    }
  }// /

  if(operand == "*"){
    for (int jj = 0; jj < pMat->ncol(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[jj][i] *= val[i];
      }
    }
  }// *

}

template <typename T>
void BigOperationByRow_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                                  String operand, NumericVector val,
                                                  NumericVector rowIndex,
                                                  NumericVector colIndex){

  // remove possible duplicates from row index
  sort( rowIndex.begin(), rowIndex.end() );
  rowIndex.erase( unique( rowIndex.begin(), rowIndex.end() ), rowIndex.end() );

  // remove possible duplicates from column index
  sort( colIndex.begin(), colIndex.end() );
  colIndex.erase( unique( colIndex.begin(), colIndex.end() ), colIndex.end() );

  if(operand == "+"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[colIndex[jj]-1][i] += val[i];
      }
    }
  }// +

  if(operand == "-"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[colIndex[jj]-1][i] -= val[i];
      }
    }
  }// -

  if(operand == "/"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[colIndex[jj]-1][i] /= val[i];
      }
    }
  }// /

  if(operand == "*"){
    for (int jj = 0; jj < colIndex.length(); jj++) {
      for (int ii = 0; ii < rowIndex.length(); ii++) {
        int i = rowIndex[ii]-1;
        mat[colIndex[jj]-1][i] *= val[i];
      }
    }
  }// *

}




/*********************** ColMeans ***********************/

// Logic for BigColMeans.
template <typename T>
NumericVector BigColMeans(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat) {

    // Create the vector we'll store the column sums in.
    NumericVector colSums(pMat->ncol());

    // Compute the colSums
    for (size_t i=0; i < pMat->ncol(); ++i){
        colSums[i] = std::accumulate(mat[i], mat[i]+pMat->nrow(), 0.0);
    }

    // Divide the colSums by the number of rows
    return colSums / pMat->nrow();
}


// Logic for BigColMeans_ColInd.
template <typename T>
NumericVector BigColMeans_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(colIndex.length());

  // for each column in the "colIndex"
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int i = colIndex[jj] - 1; // compute the index of that column in the "bootstrap". Index in R are 1-based, so remove 1.
    colSums[jj] = std::accumulate(mat[i], mat[i]+pMat->nrow(), 0.0);
  }

  // Divide the colSums by the number of rows
  return colSums / pMat->nrow();
}


// Logic for BigColMeans_RowInd.
template <typename T>
NumericVector BigColMeans_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex){

  // Create the vector we'll store the column sums in.
  NumericVector colSums(pMat->ncol());

  // for each column in the matrix
  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      colSums[jj] += mat[jj][tmp_i];
    }
  }

  // Divide the colSums by the number of rows
  return colSums / rowIndex.length();

}


// Logic for BigColMeans_RowInd_ColInd.
template <typename T>
NumericVector BigColMeans_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                        NumericVector rowIndex,
                                        NumericVector colIndex){

  // Create the vector we'll store the column sums in.
  NumericVector colSums(colIndex.length());

  // for each column in the matrix
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      colSums[jj] += mat[tmp_j][tmp_i];
    }
  }

  // Divide the colSums by the number of rows
  return colSums / rowIndex.length();

}




/*********************** ColSums ***********************/

// Logic for BigColSums.
template <typename T>
NumericVector BigColSums(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(pMat->ncol());
  for (size_t i=0; i < pMat->ncol(); ++i)
    colSums[i] = std::accumulate(mat[i], mat[i]+pMat->nrow(), 0.0);
  return colSums;
}


// Logic for BigColSums_IndCol.
template <typename T>
NumericVector BigColSums_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(colIndex.length());

  // for each column in the "colIndex"
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int i = colIndex[jj] - 1; // compute the index of that column in the "bootstrap". Index in R are 1-based, so remove 1.
    colSums[jj] = std::accumulate(mat[i], mat[i]+pMat->nrow(), 0.0);
  }
  return colSums;
}


// Logic for BigColSums_RowInd.
template <typename T>
NumericVector BigColSums_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex){

  // Create the vector we'll store the column sums in.
  NumericVector colSums(pMat->ncol());

  // for each column in the matrix
  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      colSums[jj] += mat[jj][tmp_i];
    }
  }

  return colSums;

}


// Logic for BigColSums_RowInd_ColInd.
template <typename T>
NumericVector BigColSums_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                       NumericVector rowIndex,
                                       NumericVector colIndex){

  // Create the vector we'll store the column sums in.
  NumericVector colSums(colIndex.length());

  // for each column in the matrix
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      colSums[jj] += mat[tmp_j][tmp_i];
    }
  }

  return colSums;

}




/*********************** RowMeans ***********************/

template <typename T>
NumericVector BigRowMeans(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat) {

  // Create the vector we'll store the row sums in.
  NumericVector rowSums(pMat->nrow(), 0.0);

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      rowSums[ii] += mat[jj][ii];
    }
  }

  // Divide the rowSums by the number of columns
  return rowSums / pMat->ncol();
}


template <typename T>
NumericVector BigRowMeans_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex) {

  // Create the vector we'll store the row sums in.
  NumericVector rowSums(pMat->nrow(), 0.0);


  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_jj = colIndex[jj] - 1;
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      rowSums[ii] += mat[tmp_jj][ii];
    }
  }

  // Divide the rowSums by the number of columns
  return rowSums / colIndex.length();
}


template <typename T>
NumericVector BigRowMeans_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex){

  // Create the vector we'll store the row sums in.
  NumericVector rowSums(rowIndex.length(), 0.0);


  for (int jj = 0; jj < pMat->ncol(); jj++) { // for each column
    for (int ii = 0; ii < rowIndex.length(); ii++) { // for each row (in the input row index set)
      int tmp_i = rowIndex[ii] - 1;
      rowSums[ii] += mat[jj][tmp_i];
    }
  }

  // Divide the rowSums by the number of columns
  return rowSums / pMat->ncol();
}


template <typename T>
NumericVector BigRowMeans_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                        NumericVector rowIndex,
                                        NumericVector colIndex){

  // Create the vector we'll store the row sums in.
  NumericVector rowSums(rowIndex.length(), 0.0);

  for (int jj = 0; jj < colIndex.length(); jj++) { // for each column (in the input col index set)
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) { // for each row (in the input row index set)
      int tmp_i = rowIndex[ii] - 1;
      rowSums[ii] += mat[tmp_j][tmp_i];
    }
  }

  // Divide the rowSums by the number of columns
  return rowSums / colIndex.length();
}




/*********************** RowSums ***********************/

template <typename T>
NumericVector BigRowSums(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat) {

  NumericVector rowSums(pMat->nrow(), 0.0);
  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      rowSums[ii] += mat[jj][ii];
    }
  }
  return rowSums;
}


template <typename T>
NumericVector BigRowSums_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex) {

  NumericVector rowSums(pMat->nrow(), 0.0);

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_jj = colIndex[jj] - 1;
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      rowSums[ii] += mat[tmp_jj][ii];
    }
  }
  return rowSums;
}


template <typename T>
NumericVector BigRowSums_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex){

  NumericVector rowSums(rowIndex.length(), 0.0);

  for (int jj = 0; jj < pMat->ncol(); jj++) { // for each column
    for (int ii = 0; ii < rowIndex.length(); ii++) { // for each row (in the input row index set)
      int tmp_i = rowIndex[ii] - 1;
      rowSums[ii] += mat[jj][tmp_i];
    }
  }
  return rowSums;
}


template <typename T>
NumericVector BigRowSums_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                       NumericVector rowIndex,
                                       NumericVector colIndex){

  NumericVector rowSums(rowIndex.length(), 0.0);


  for (int jj = 0; jj < colIndex.length(); jj++) { // for each column
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) { // for each row (in the input row index set)
      int tmp_i = rowIndex[ii] - 1;
      rowSums[ii] += mat[tmp_j][tmp_i];
    }
  }


  return rowSums;
}




/*********************** Mean ***********************/

// Logic for BigMeans.
template <typename T>
double BigMeans(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(pMat->ncol());
  double mean_val = 0.0;
  for (size_t i=0; i < pMat->ncol(); ++i)
    colSums[i] = std::accumulate(mat[i], mat[i]+pMat->nrow(), 0.0);
  colSums = colSums / pMat->nrow();
  return( (std::accumulate(colSums.begin(), colSums.end(), 0.0)) / pMat->ncol() );
}

// Logic for BigMeans_ColInd.
template <typename T>
double BigMeans_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(colIndex.length());

  for (size_t i=0; i < colIndex.length(); ++i){
    size_t tmp_i = colIndex[i] - 1;
    colSums[i] = std::accumulate(mat[tmp_i], mat[tmp_i]+pMat->nrow(), 0.0);
  }
  colSums = colSums / pMat->nrow();
  return( (std::accumulate(colSums.begin(), colSums.end(), 0.0)) / colIndex.length() );


}

// Logic for BigMeans_RowInd.
template <typename T>
double BigMeans_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(pMat->ncol(), 0.0);

  // for each column in the matrix
  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      colSums[jj] += mat[jj][tmp_i];
    }
  }

  // colmeans
  colSums = colSums / rowIndex.length();

  // mean of colmeans
  return( (std::accumulate(colSums.begin(), colSums.end(), 0.0)) / pMat->ncol() );
}

// Logic for BigMeans_RowInd_ColInd.
template <typename T>
double BigMeans_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                              NumericVector rowIndex,
                              NumericVector colIndex) {

  // Create the vector we'll store the column sums in.
  NumericVector colSums(colIndex.length(), 0.0);

  // for each column in the matrix
  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      colSums[jj] += mat[tmp_j][tmp_i];
    }
  }

  // colmeans
  colSums = colSums / rowIndex.length();

  // mean of colmeans
  return( (std::accumulate(colSums.begin(), colSums.end(), 0.0)) / colIndex.length() );
}




/*********************** Sd ***********************/

// Logic for BigSd
template <typename T>
double BigSd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat) {

  double K = BigMeans(pMat, mat);

  double n = pMat->nrow() * pMat->ncol();
  double ex = 0.0;
  double ex2 = 0.0;

  double sd = 0.0;

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      ex += mat[jj][ii] - K;
      ex2 += (mat[jj][ii] - K)*(mat[jj][ii] - K);
    }
  }

  sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );

  return sd;

}

// Logic for BigSd_ColInd
template <typename T>
double BigSd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex) {

  double K = BigMeans_ColInd(pMat, mat, colIndex);

  double n = pMat->nrow() * colIndex.length();
  double ex = 0.0;
  double ex2 = 0.0;

  double sd = 0.0;

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      ex += mat[tmp_j][ii] - K;
      ex2 += (mat[tmp_j][ii] - K)*(mat[tmp_j][ii] - K);
    }
  }

  sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );

  return sd;

}

// Logic for BigSd_RowInd
template <typename T>
double BigSd_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex) {

  double K = BigMeans_RowInd(pMat, mat, rowIndex);

  double n = rowIndex.length() * pMat->ncol();
  double ex = 0.0;
  double ex2 = 0.0;

  double sd = 0.0;

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      ex += mat[jj][tmp_i] - K;
      ex2 += (mat[jj][tmp_i] - K)*(mat[jj][tmp_i] - K);
    }
  }

  sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );

  return sd;

}

// Logic for BigSd_RowInd_ColInd
template <typename T>
double BigSd_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                           NumericVector rowIndex,
                           NumericVector colIndex) {

  double K = BigMeans_RowInd_ColInd(pMat, mat, rowIndex, colIndex);

  double n = rowIndex.length() * colIndex.length();
  double ex = 0.0;
  double ex2 = 0.0;

  double sd = 0.0;

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      ex += mat[tmp_j][tmp_i] - K;
      ex2 += (mat[tmp_j][tmp_i] - K)*(mat[tmp_j][tmp_i] - K);
    }
  }

  sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );

  return sd;

}




/*********************** RowSd ***********************/

// Logic for BigRowSd
template <typename T>
NumericVector BigRowSd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat){

  // compute the mean of each row
  NumericVector rowMeans = BigRowMeans(pMat, mat);

  double n = pMat->ncol() ;

  NumericVector ex(pMat->nrow(), 0.0);
  NumericVector ex2(pMat->nrow(), 0.0);

  NumericVector sd(pMat->nrow(), 0.0);

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      double K = rowMeans[ii];
      ex[ii] += mat[jj][ii] - K;
      ex2[ii] += (mat[jj][ii] - K)*(mat[jj][ii] - K);
    }
  }

  for (int ii = 0; ii < pMat->nrow(); ii++) {
    sd[ii] = sqrt( (ex2[ii] - (ex[ii]*ex[ii]) / n) / (n - 1.0) );
  }

  return sd;

}

// Logic for BigRowSd_ColInd
template <typename T>
NumericVector BigRowSd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex){

  // compute the mean of each row
  NumericVector rowMeans = BigRowMeans_ColInd(pMat, mat, colIndex);

  double n = colIndex.length() ;

  NumericVector ex(pMat->nrow(), 0.0);
  NumericVector ex2(pMat->nrow(), 0.0);

  NumericVector sd(pMat->nrow(), 0.0);

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      double K = rowMeans[ii];
      ex[ii] += mat[tmp_j][ii] - K;
      ex2[ii] += (mat[tmp_j][ii] - K)*(mat[tmp_j][ii] - K);
    }
  }

  for (int ii = 0; ii < pMat->nrow(); ii++) {
    sd[ii] = sqrt( (ex2[ii] - (ex[ii]*ex[ii]) / n) / (n - 1.0) );
  }

  return sd;

}

// Logic for BigRowSd_RowInd
template <typename T>
NumericVector BigRowSd_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex){

  // compute the mean of each row
  NumericVector rowMeans = BigRowMeans_RowInd(pMat, mat, rowIndex);

  double n = pMat->ncol() ;

  NumericVector ex(rowIndex.length(), 0.0);
  NumericVector ex2(rowIndex.length(), 0.0);

  NumericVector sd(rowIndex.length(), 0.0);

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      double K = rowMeans[ii];
      ex[ii] += mat[jj][tmp_i] - K;
      ex2[ii] += (mat[jj][tmp_i] - K)*(mat[jj][tmp_i] - K);
    }
  }

  for (int ii = 0; ii < rowIndex.length(); ii++) {
    sd[ii] = sqrt( (ex2[ii] - (ex[ii]*ex[ii]) / n) / (n - 1.0) );
  }

  return sd;

}

// Logic for BigRowSd_RowInd_ColInd
template <typename T>
NumericVector BigRowSd_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                     NumericVector rowIndex,
                                     NumericVector colIndex){

  // compute the mean of each row
  NumericVector rowMeans = BigRowMeans_RowInd_ColInd(pMat, mat, rowIndex, colIndex);

  double n = colIndex.length();

  NumericVector ex(rowIndex.length(), 0.0);
  NumericVector ex2(rowIndex.length(), 0.0);

  NumericVector sd(rowIndex.length(), 0.0);

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      double K = rowMeans[ii];
      ex[ii] += mat[tmp_j][tmp_i] - K;
      ex2[ii] += (mat[tmp_j][tmp_i] - K)*(mat[tmp_j][tmp_i] - K);
    }
  }

  for (int ii = 0; ii < rowIndex.length(); ii++) {
    sd[ii] = sqrt( (ex2[ii] - (ex[ii]*ex[ii]) / n) / (n - 1.0) );
  }

  return sd;

}




/*********************** ColSd ***********************/

// Logic for BigColSd
template <typename T>
NumericVector BigColSd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat){

  // compute the mean of each column
  NumericVector colMeans = BigColMeans(pMat, mat);

  double n = pMat->nrow();

  NumericVector ex(pMat->ncol(), 0.0);
  NumericVector ex2(pMat->ncol(), 0.0);

  NumericVector sd(pMat->ncol(), 0.0);

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    double K = colMeans[jj];
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      ex[jj] += mat[jj][ii] - K;
      ex2[jj] += (mat[jj][ii] - K)*(mat[jj][ii] - K);
    }
  }

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    sd[jj] = sqrt( (ex2[jj] - (ex[jj]*ex[jj]) / n) / (n - 1.0) );
  }

  return sd;

}


// Logic for BigColSd_ColInd
template <typename T>
NumericVector BigColSd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector colIndex){

  // compute the mean of each column
  NumericVector colMeans = BigColMeans_ColInd(pMat, mat, colIndex);

  double n = pMat->nrow();

  NumericVector ex(colIndex.length(), 0.0);
  NumericVector ex2(colIndex.length(), 0.0);

  NumericVector sd(colIndex.length(), 0.0);

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    double K = colMeans[jj];
    for (int ii = 0; ii < pMat->nrow(); ii++) {
      ex[jj] += mat[tmp_j][ii] - K;
      ex2[jj] += (mat[tmp_j][ii] - K)*(mat[tmp_j][ii] - K);
    }
  }

  for (int jj = 0; jj < colIndex.length(); jj++) {
    sd[jj] = sqrt( (ex2[jj] - (ex[jj]*ex[jj]) / n) / (n - 1.0) );
  }

  return sd;

}


// Logic for BigColSd_RowInd
template <typename T>
NumericVector BigColSd_RowInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat, NumericVector rowIndex){

  // compute the mean of each column
  NumericVector colMeans = BigColMeans_RowInd(pMat, mat, rowIndex);

  double n = rowIndex.length();

  NumericVector ex(pMat->ncol(), 0.0);
  NumericVector ex2(pMat->ncol(), 0.0);

  NumericVector sd(pMat->ncol(), 0.0);

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    double K = colMeans[jj];
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      ex[jj] += mat[jj][tmp_i] - K;
      ex2[jj] += (mat[jj][tmp_i] - K)*(mat[jj][tmp_i] - K);
    }
  }

  for (int jj = 0; jj < pMat->ncol(); jj++) {
    sd[jj] = sqrt( (ex2[jj] - (ex[jj]*ex[jj]) / n) / (n - 1.0) );
  }

  return sd;

}


// Logic for BigColSd_RowInd_ColInd
template <typename T>
NumericVector BigColSd_RowInd_ColInd(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
                                     NumericVector rowIndex, NumericVector colIndex){

  // compute the mean of each column
  NumericVector colMeans = BigColMeans_RowInd_ColInd(pMat, mat, rowIndex, colIndex);

  double n = rowIndex.length();

  NumericVector ex(colIndex.length(), 0.0);
  NumericVector ex2(colIndex.length(), 0.0);

  NumericVector sd(colIndex.length(), 0.0);

  for (int jj = 0; jj < colIndex.length(); jj++) {
    int tmp_j = colIndex[jj] - 1;
    double K = colMeans[jj];
    for (int ii = 0; ii < rowIndex.length(); ii++) {
      int tmp_i = rowIndex[ii] - 1;
      ex[jj] += mat[tmp_j][tmp_i] - K;
      ex2[jj] += (mat[tmp_j][tmp_i] - K)*(mat[tmp_j][tmp_i] - K);
    }
  }

  for (int jj = 0; jj < colIndex.length(); jj++) {
    sd[jj] = sqrt( (ex2[jj] - (ex[jj]*ex[jj]) / n) / (n - 1.0) );
  }

  return sd;

}






/*********************** ColMeans ***********************/

// Dispatch function for BigColMeans
//
// [[Rcpp::export]]
NumericVector BigColMeans(SEXP pBigMat) {
    // First we have to tell Rcpp what class to use for big.matrix objects.
    // This object stores the attributes of the big.matrix object passed to it
    // by R.
    XPtr<BigMatrix> xpMat(pBigMat);

    // To access values in the big.matrix, we need to create a MatrixAccessor
    // object of the appropriate type. Note that in every case we are still
    // returning a NumericVector: this is because big.matrix objects only store
    // numeric values in R, even if their type is set to 'char'. The types
    // simply correspond to the number of bytes used for each element.
    switch(xpMat->matrix_type()) {
      case 1:
        return BigColMeans(xpMat, MatrixAccessor<char>(*xpMat));
      case 2:
        return BigColMeans(xpMat, MatrixAccessor<short>(*xpMat));
      case 4:
        return BigColMeans(xpMat, MatrixAccessor<int>(*xpMat));
      case 6:
        return BigColMeans(xpMat, MatrixAccessor<float>(*xpMat));
      case 8:
        return BigColMeans(xpMat, MatrixAccessor<double>(*xpMat));
      default:
        // This case should never be encountered unless the implementation of
        // big.matrix changes, but is necessary to implement shut up compiler
        // warnings.
        throw Rcpp::exception("unknown type detected for big.matrix object!");
    }
}


// Dispatch function for BigColMeans_ColInd
//
// [[Rcpp::export]]
NumericVector BigColMeans_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColMeans_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigColMeans_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigColMeans_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigColMeans_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigColMeans_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColMeans_RowInd
//
// [[Rcpp::export]]
NumericVector BigColMeans_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColMeans_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigColMeans_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigColMeans_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigColMeans_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigColMeans_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}



// Dispatch function for BigColMeans_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigColMeans_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColMeans_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigColMeans_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigColMeans_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigColMeans_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigColMeans_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}







/*********************** ColSums ***********************/

// Dispatch function for BigColSums
//
// [[Rcpp::export]]
NumericVector BigColSums(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSums(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigColSums(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigColSums(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigColSums(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigColSums(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColSums_ColInd
//
// [[Rcpp::export]]
NumericVector BigColSums_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSums_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigColSums_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigColSums_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigColSums_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigColSums_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColSums_RowInd
//
// [[Rcpp::export]]
NumericVector BigColSums_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSums_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigColSums_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigColSums_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigColSums_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigColSums_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColSums_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigColSums_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSums_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigColSums_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigColSums_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigColSums_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigColSums_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}







/*********************** RowMeans ***********************/

// Dispatch function for BigRowMeans
//
// [[Rcpp::export]]
NumericVector BigRowMeans(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowMeans(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigRowMeans(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigRowMeans(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigRowMeans(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigRowMeans(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigRowMeans_ColInd
//
// [[Rcpp::export]]
NumericVector BigRowMeans_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowMeans_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigRowMeans_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigRowMeans_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigRowMeans_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigRowMeans_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigRowMeans_RowInd
//
// [[Rcpp::export]]
NumericVector BigRowMeans_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowMeans_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigRowMeans_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigRowMeans_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigRowMeans_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigRowMeans_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigRowMeans_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigRowMeans_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowMeans_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigRowMeans_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigRowMeans_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigRowMeans_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigRowMeans_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}





/*********************** RowSums ***********************/


// Dispatch function for BigRowSums
//
// [[Rcpp::export]]
NumericVector BigRowSums(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSums(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigRowSums(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigRowSums(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigRowSums(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigRowSums(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigRowSums_ColInd
//
// [[Rcpp::export]]
NumericVector BigRowSums_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSums_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigRowSums_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigRowSums_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigRowSums_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigRowSums_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigRowSums_RowInd
//
// [[Rcpp::export]]
NumericVector BigRowSums_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSums_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigRowSums_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigRowSums_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigRowSums_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigRowSums_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigRowSums_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigRowSums_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSums_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigRowSums_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigRowSums_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigRowSums_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigRowSums_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}







/*********************** BigMean ***********************/


// Dispatch function for BigMeans
//
// [[Rcpp::export]]
double BigMeans(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigMeans(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigMeans(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigMeans(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigMeans(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigMeans(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigMeans_ColInd
//
// [[Rcpp::export]]
double BigMeans_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigMeans_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigMeans_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigMeans_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigMeans_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigMeans_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigMeans_RowInd
//
// [[Rcpp::export]]
double BigMeans_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigMeans_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigMeans_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigMeans_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigMeans_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigMeans_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigMeans_RowInd_ColInd
//
// [[Rcpp::export]]
double BigMeans_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigMeans_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigMeans_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigMeans_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigMeans_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigMeans_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}







/*********************** BigSd ***********************/


// Dispatch function for BigSd
//
// [[Rcpp::export]]
double BigSd(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigSd(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigSd(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigSd(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigSd(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigSd(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigSd_ColInd
//
// [[Rcpp::export]]
double BigSd_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigSd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigSd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigSd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigSd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigSd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigSd_RowInd
//
// [[Rcpp::export]]
double BigSd_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigSd_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigSd_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigSd_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigSd_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigSd_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigSd_RowInd_ColInd
//
// [[Rcpp::export]]
double BigSd_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigSd_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigSd_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigSd_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigSd_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigSd_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}



/*********************** BigRowSd ***********************/


// Dispatch function for BigRowSd
//
// [[Rcpp::export]]
NumericVector BigRowSd(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSd(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigRowSd(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigRowSd(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigRowSd(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigRowSd(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigRowSd_ColInd
//
// [[Rcpp::export]]
NumericVector BigRowSd_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigRowSd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigRowSd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigRowSd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigRowSd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigRowSd_RowInd
//
// [[Rcpp::export]]
NumericVector BigRowSd_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSd_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigRowSd_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigRowSd_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigRowSd_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigRowSd_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigRowSd_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigRowSd_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigRowSd_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigRowSd_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigRowSd_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigRowSd_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigRowSd_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}




/*********************** BigColSd ***********************/


// Dispatch function for BigColSd
//
// [[Rcpp::export]]
NumericVector BigColSd(SEXP pBigMat) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSd(xpMat, MatrixAccessor<char>(*xpMat));
  case 2:
    return BigColSd(xpMat, MatrixAccessor<short>(*xpMat));
  case 4:
    return BigColSd(xpMat, MatrixAccessor<int>(*xpMat));
  case 6:
    return BigColSd(xpMat, MatrixAccessor<float>(*xpMat));
  case 8:
    return BigColSd(xpMat, MatrixAccessor<double>(*xpMat));
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColSd_ColInd
//
// [[Rcpp::export]]
NumericVector BigColSd_ColInd(SEXP pBigMat, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), col_ind);
  case 2:
    return BigColSd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), col_ind);
  case 4:
    return BigColSd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), col_ind);
  case 6:
    return BigColSd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), col_ind);
  case 8:
    return BigColSd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColSd_RowInd
//
// [[Rcpp::export]]
NumericVector BigColSd_RowInd(SEXP pBigMat, NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSd_RowInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind);
  case 2:
    return BigColSd_RowInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind);
  case 4:
    return BigColSd_RowInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind);
  case 6:
    return BigColSd_RowInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind);
  case 8:
    return BigColSd_RowInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// Dispatch function for BigColSd_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigColSd_RowInd_ColInd(SEXP pBigMat, NumericVector row_ind, NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigColSd_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), row_ind, col_ind);
  case 2:
    return BigColSd_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), row_ind, col_ind);
  case 4:
    return BigColSd_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), row_ind, col_ind);
  case 6:
    return BigColSd_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), row_ind, col_ind);
  case 8:
    return BigColSd_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}





/*********************** BigBootstrap ***********************/


// Dispatch function for BigBootstrap
//
// [[Rcpp::export]]
List BigBootstrap(SEXP pBigMat,
                  NumericVector bootstrap_ind,
                  NumericVector H0_genes,
                  NumericVector ligands,
                  NumericVector receptors) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigBootstrap(xpMat, MatrixAccessor<char>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 2:
    return BigBootstrap(xpMat, MatrixAccessor<short>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 4:
    return BigBootstrap(xpMat, MatrixAccessor<int>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 6:
    return BigBootstrap(xpMat, MatrixAccessor<float>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 8:
    return BigBootstrap(xpMat, MatrixAccessor<double>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}



// Dispatch function for BigBootstrap_without_zeros
//
// [[Rcpp::export]]
List BigBootstrap_without_zeros(SEXP pBigMat,
                                NumericVector bootstrap_ind,
                                NumericVector H0_genes,
                                NumericVector ligands,
                                NumericVector receptors) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigBootstrap_without_zeros(xpMat, MatrixAccessor<char>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 2:
    return BigBootstrap_without_zeros(xpMat, MatrixAccessor<short>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 4:
    return BigBootstrap_without_zeros(xpMat, MatrixAccessor<int>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 6:
    return BigBootstrap_without_zeros(xpMat, MatrixAccessor<float>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  case 8:
    return BigBootstrap_without_zeros(xpMat, MatrixAccessor<double>(*xpMat), bootstrap_ind, H0_genes, ligands, receptors);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}








/*********************** BigCountElementByRow ***********************/


// Dispatch function for BigCountElementByRow
//
// [[Rcpp::export]]
NumericVector BigCountElementByRow(SEXP pBigMat,
                                   String operand,
                                   double val) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigCountElementByRow(xpMat, MatrixAccessor<char>(*xpMat), operand, val);
  case 2:
    return BigCountElementByRow(xpMat, MatrixAccessor<short>(*xpMat), operand, val);
  case 4:
    return BigCountElementByRow(xpMat, MatrixAccessor<int>(*xpMat), operand, val);
  case 6:
    return BigCountElementByRow(xpMat, MatrixAccessor<float>(*xpMat), operand, val);
  case 8:
    return BigCountElementByRow(xpMat, MatrixAccessor<double>(*xpMat), operand, val);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigCountElementByRow_RowInd
//
// [[Rcpp::export]]
NumericVector BigCountElementByRow_RowInd(SEXP pBigMat,
                                          String operand,
                                          double val,
                                          NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigCountElementByRow_RowInd(xpMat, MatrixAccessor<char>(*xpMat), operand, val, row_ind);
  case 2:
    return BigCountElementByRow_RowInd(xpMat, MatrixAccessor<short>(*xpMat), operand, val, row_ind);
  case 4:
    return BigCountElementByRow_RowInd(xpMat, MatrixAccessor<int>(*xpMat), operand, val, row_ind);
  case 6:
    return BigCountElementByRow_RowInd(xpMat, MatrixAccessor<float>(*xpMat), operand, val, row_ind);
  case 8:
    return BigCountElementByRow_RowInd(xpMat, MatrixAccessor<double>(*xpMat), operand, val, row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigCountElementByRow_ColInd
//
// [[Rcpp::export]]
NumericVector BigCountElementByRow_ColInd(SEXP pBigMat,
                                          String operand,
                                          double val,
                                          NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigCountElementByRow_ColInd(xpMat, MatrixAccessor<char>(*xpMat), operand, val, col_ind);
  case 2:
    return BigCountElementByRow_ColInd(xpMat, MatrixAccessor<short>(*xpMat), operand, val, col_ind);
  case 4:
    return BigCountElementByRow_ColInd(xpMat, MatrixAccessor<int>(*xpMat), operand, val, col_ind);
  case 6:
    return BigCountElementByRow_ColInd(xpMat, MatrixAccessor<float>(*xpMat), operand, val, col_ind);
  case 8:
    return BigCountElementByRow_ColInd(xpMat, MatrixAccessor<double>(*xpMat), operand, val, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigCountElementByRow_RowInd_ColInd
//
// [[Rcpp::export]]
NumericVector BigCountElementByRow_RowInd_ColInd(SEXP pBigMat,
                                          String operand,
                                          double val,
                                          NumericVector row_ind,
                                          NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 1:
    return BigCountElementByRow_RowInd_ColInd(xpMat, MatrixAccessor<char>(*xpMat), operand, val, row_ind, col_ind);
  case 2:
    return BigCountElementByRow_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), operand, val, row_ind, col_ind);
  case 4:
    return BigCountElementByRow_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), operand, val, row_ind, col_ind);
  case 6:
    return BigCountElementByRow_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), operand, val, row_ind, col_ind);
  case 8:
    return BigCountElementByRow_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), operand, val, row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}





/*********************** BigOperationByRow ***********************/


// Dispatch function for BigOperationByRow
//
// [[Rcpp::export]]
void BigOperationByRow(SEXP pBigMat,
                       String operand,
                       NumericVector val) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  //  cout << xpMat->matrix_type() << endl;

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  // cout << xpMat->matrix_type() << "\n";

  switch(xpMat->matrix_type()) {
  case 2:
    return BigOperationByRow(xpMat, MatrixAccessor<short>(*xpMat), operand, val);
  case 4:
    return BigOperationByRow(xpMat, MatrixAccessor<int>(*xpMat), operand, val);
  case 6:
    return BigOperationByRow(xpMat, MatrixAccessor<float>(*xpMat), operand, val);
  case 8:
    return BigOperationByRow(xpMat, MatrixAccessor<double>(*xpMat), operand, val);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigOperationByRow_RowInd
//
// [[Rcpp::export]]
void BigOperationByRow_RowInd(SEXP pBigMat,
                                      String operand,
                                      NumericVector val,
                                      NumericVector row_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 2:
    return BigOperationByRow_RowInd(xpMat, MatrixAccessor<short>(*xpMat), operand, val, row_ind);
  case 4:
    return BigOperationByRow_RowInd(xpMat, MatrixAccessor<int>(*xpMat), operand, val, row_ind);
  case 6:
    return BigOperationByRow_RowInd(xpMat, MatrixAccessor<float>(*xpMat), operand, val, row_ind);
  case 8:
    return BigOperationByRow_RowInd(xpMat, MatrixAccessor<double>(*xpMat), operand, val, row_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigOperationByRow_ColInd
//
// [[Rcpp::export]]
void BigOperationByRow_ColInd(SEXP pBigMat,
                                      String operand,
                                      NumericVector val,
                                      NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 2:
    return BigOperationByRow_ColInd(xpMat, MatrixAccessor<short>(*xpMat), operand, val, col_ind);
  case 4:
    return BigOperationByRow_ColInd(xpMat, MatrixAccessor<int>(*xpMat), operand, val, col_ind);
  case 6:
    return BigOperationByRow_ColInd(xpMat, MatrixAccessor<float>(*xpMat), operand, val, col_ind);
  case 8:
    return BigOperationByRow_ColInd(xpMat, MatrixAccessor<double>(*xpMat), operand, val, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}

// Dispatch function for BigOperationByRow_RowInd_ColInd
//
// [[Rcpp::export]]
void BigOperationByRow_RowInd_ColInd(SEXP pBigMat,
                                        String operand,
                                        NumericVector val,
                                        NumericVector row_ind,
                                        NumericVector col_ind) {
  // First we have to tell Rcpp what class to use for big.matrix objects.
  // This object stores the attributes of the big.matrix object passed to it
  // by R.
  XPtr<BigMatrix> xpMat(pBigMat);

  // To access values in the big.matrix, we need to create a MatrixAccessor
  // object of the appropriate type. Note that in every case we are still
  // returning a NumericVector: this is because big.matrix objects only store
  // numeric values in R, even if their type is set to 'char'. The types
  // simply correspond to the number of bytes used for each element.
  switch(xpMat->matrix_type()) {
  case 2:
    return BigOperationByRow_RowInd_ColInd(xpMat, MatrixAccessor<short>(*xpMat), operand, val, row_ind, col_ind);
  case 4:
    return BigOperationByRow_RowInd_ColInd(xpMat, MatrixAccessor<int>(*xpMat), operand, val, row_ind, col_ind);
  case 6:
    return BigOperationByRow_RowInd_ColInd(xpMat, MatrixAccessor<float>(*xpMat), operand, val, row_ind, col_ind);
  case 8:
    return BigOperationByRow_RowInd_ColInd(xpMat, MatrixAccessor<double>(*xpMat), operand, val, row_ind, col_ind);
  default:
    // This case should never be encountered unless the implementation of
    // big.matrix changes, but is necessary to implement shut up compiler
    // warnings.
    throw Rcpp::exception("unknown type detected for big.matrix object!");
  }
}


// // Logic
// template <typename T>
// NumericVector BigWilc(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat,
//                       NumericVector rowIndex,
//                       NumericVector colIndex1,
//                       NumericVector colIndex2){
//
//   Rcpp::Environment stats("package:stats");
//
//   // Making R function callable from C++
//   Rcpp::Function wilcox_test = stats["wilcox.test"];
//
//   NumericVector pvalues(rowIndex.length(), 0.0);
//
//   for (int rr = 0; rr < rowIndex.length(); rr++) {
//   //   //NumericVector tmp1 = BigRow_ColInd(pMat, rowIndex[rr]-1, colIndex1) ;
//
//      int ii = rowIndex[rr] -1 ;
//
//      // NumericVector tmp1(colIndex1.length(), 0.0);
//      NumericVector tmp1;
//
//     for (int jj = 0; jj < colIndex1.length(); jj++) {
//        int tmp_j = colIndex1[jj] - 1;
//        //tmp1[jj] = mat[tmp_j][ii];
//        tmp1.push_back(mat[tmp_j][ii]);
//      }
//
//     //NumericVector tmp2(colIndex2.length(), 0.0);
//     NumericVector tmp2;
//
//     for (int jj = 0; jj < colIndex2.length(); jj++) {
//       int tmp_j = colIndex2[jj] - 1;
//       //tmp2[jj] = mat[tmp_j][ii];
//       tmp2.push_back(mat[tmp_j][ii]);
//     }
//
//
//     //std::cout << Rcpp::is<List>(wilcox_test(Rcpp::_["x"] = tmp1, Rcpp::_["y"] = tmp2, Rcpp::_["exact"] = false));
//
//     Rcpp::List pvalue = wilcox_test(Rcpp::_["x"] = tmp1, Rcpp::_["y"] = tmp2, Rcpp::_["exact"] = false);
//
//     pvalues[rr] = pvalue["p.value"];
//
//   }
//
//   return pvalues;
//
// }
//
//
//
// // Dispatch function for BigWilc
// //
// // [[Rcpp::export]]
// NumericVector BigWilc(SEXP pBigMat,
//                       NumericVector rowIndex,
//                       NumericVector colIndex1,
//                       NumericVector colIndex2) {
//   // First we have to tell Rcpp what class to use for big.matrix objects.
//   // This object stores the attributes of the big.matrix object passed to it
//   // by R.
//   XPtr<BigMatrix> xpMat(pBigMat);
//
//   // To access values in the big.matrix, we need to create a MatrixAccessor
//   // object of the appropriate type. Note that in every case we are still
//   // returning a NumericVector: this is because big.matrix objects only store
//   // numeric values in R, even if their type is set to 'char'. The types
//   // simply correspond to the number of bytes used for each element.
//   switch(xpMat->matrix_type()) {
//   case 1:
//     return BigWilc(xpMat, MatrixAccessor<char>(*xpMat), rowIndex, colIndex1, colIndex2);
//   case 2:
//     return BigWilc(xpMat, MatrixAccessor<short>(*xpMat), rowIndex, colIndex1, colIndex2);
//   case 4:
//     return BigWilc(xpMat, MatrixAccessor<int>(*xpMat), rowIndex, colIndex1, colIndex2);
//   case 6:
//     return BigWilc(xpMat, MatrixAccessor<float>(*xpMat), rowIndex, colIndex1, colIndex2);
//   case 8:
//     return BigWilc(xpMat, MatrixAccessor<double>(*xpMat), rowIndex, colIndex1, colIndex2);
//   default:
//     // This case should never be encountered unless the implementation of
//     // big.matrix changes, but is necessary to implement shut up compiler
//     // warnings.
//     throw Rcpp::exception("unknown type detected for big.matrix object!");
//   }
// }







