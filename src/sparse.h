#include <Rcpp.h>
#include <RcppCommon.h>
//[[Rcpp::plugins(openmp)]]

// [[Rcpp::depends(bigmemory, BH)]]
#include <bigmemory/MatrixAccessor.hpp>
#include <Rcpp.h>
using namespace Rcpp;

#ifdef _OPENMP
#include <omp.h>
#endif
// [[Rcpp::plugins(cpp11)]]

// namespace MySparse {
//   class MySparseMatrix;
// }

// namespace MySparse {
class MySparseMatrix {
public:
  // fields
  Rcpp::NumericVector x;
  Rcpp::IntegerVector i, p, Dim;

  // constructor
  MySparseMatrix(const Rcpp::S4& s) {
    if (!s.hasSlot("x") || !s.hasSlot("p") || !s.hasSlot("i") || !s.hasSlot("Dim"))
      throw std::invalid_argument("Invalid S4 object, it must be a dgCMatrix");
    x = s.slot("x");
    i = s.slot("i");
    p = s.slot("p");
    Dim = s.slot("Dim");
  }

  int nrow() { return Dim[0]; };
  int ncol() { return Dim[1]; };

  Rcpp::NumericVector rowSumsColIndex(Rcpp::NumericVector colIndex) {
    Rcpp::NumericVector sums(Dim[0]);
    const int NCOL = colIndex.size();
    int col;
    for (int jcol = 0; jcol < NCOL; ++jcol){
      col = colIndex[jcol] - 1; // remove 1 since R indices (input) are 1-based
      for (int j = p[col]; j < p[col + 1]; ++j){
        sums[i[j]] += x[j];
      }
    }
    return sums;
  } // method rowSumsColIndex


  Rcpp::NumericVector sparseCountElementByRow_ColInd(Rcpp::NumericVector colIndex, double count_thr) {
    Rcpp::NumericVector sums(Dim[0]);
    const int NCOL = colIndex.size();
    int col;
    for (int jcol = 0; jcol < NCOL; ++jcol){
      col = colIndex[jcol] - 1; // remove 1 since R indices (input) are 1-based
      for (int j = p[col]; j < p[col + 1]; ++j){
        if(x[j] > count_thr){
          sums[i[j]] += 1;
        }

      }
    }
    return sums;
  } // method rowSumsColIndex


  Rcpp::NumericVector sparseCountElementByRow_RowInd_ColInd(Rcpp::NumericVector rowIndex, Rcpp::NumericVector colIndex, double count_thr) {

    Rcpp::NumericVector sums(Dim[0]);
    const int NCOL = colIndex.size();
    int col;
    for (int jcol = 0; jcol < NCOL; ++jcol){
      col = colIndex[jcol] - 1; // remove 1 since R indices (input) are 1-based
      for (int j = p[col]; j < p[col + 1]; ++j){
        if(x[j] > count_thr){
          sums[i[j]] += 1;
        }

      }
    }
    return sums[rowIndex -1];
  } // method sparseCountElementByRow_RowInd_ColInd



  /////////////////////////

  // DataFrame sparseWilc(Rcpp::NumericVector rowIndex,
  //                                Rcpp::NumericVector col_id,
  //                                Rcpp::NumericVector ref_col_id) {
  //
  //   Rcpp::NumericVector pvalues(pMat->nrow(), 1.0);
  //   Rcpp::NumericVector sign(pMat->nrow());
  //   sign.fill(NA_REAL); // NA_REAL is a constant representing NaN in Rcpp
  //
  //
  // }


  //////////////////////////

  Rcpp::NumericVector rowMeansRowColIndex(Rcpp::NumericVector rowIndex, Rcpp::NumericVector colIndex) {
    Rcpp::NumericVector means = rowMeansColIndex(colIndex);
    Rcpp::NumericVector row_means = means[rowIndex-1];
    return row_means;
  } // method rowMeansRowColIndex


  double MeanSparse() {
    double Mean = std::accumulate(x.begin(), x.end(), 0.0);
    return Mean/(Dim[0]*Dim[1]);
  } // method MeanSparse


  Rcpp::NumericVector rowMeansColIndex(Rcpp::NumericVector colIndex) {
    Rcpp::NumericVector sums = rowSumsColIndex(colIndex) / colIndex.size();
    return sums;
  } // method rowMeansColIndex


  double MeanSparseColIndex(Rcpp::NumericVector colIndex) {
    Rcpp::NumericVector geneMeans = rowMeansColIndex(colIndex);
    double mean = ( std::accumulate(geneMeans.begin(), geneMeans.end(), 0.0) ) / geneMeans.length();
    return mean;
  } // method MeanSparseColIndex

  Rcpp::List BigBootstrap(Rcpp::NumericVector colIndex,
                          Rcpp::NumericVector H0_genes,
                          Rcpp::NumericVector ligands,
                          Rcpp::NumericVector receptors){

    const int N_COL = colIndex.length();
    const int N_GENES = H0_genes.length();
    const int N_LIG = ligands.length();
    const int N_REC = receptors.length();

    // compute the mean of all genes
    Rcpp::NumericVector geneMeans = rowMeansColIndex(colIndex);

    // compute ligands and receptors means (subset of all genes mean)
    Rcpp::NumericVector ligands_avg = geneMeans[ligands-1];
    Rcpp::NumericVector receptors_avg = geneMeans[receptors-1];

    // compute H0 genes means (subset of all genes mean) to be used in matrix mean computation
    Rcpp::NumericVector H0geneMeans = geneMeans[H0_genes-1];
    // compute matrix mean as the mean of genes means
    double H0_mean = ( std::accumulate(H0geneMeans.begin(), H0geneMeans.end(), 0.0) ) / N_GENES;


    double H0_sd = 0.0;
    double K = H0_mean; // shift parameter
    double n = N_GENES * N_COL; // total number of elements
    Rcpp::NumericVector exv(Dim[0]);
    Rcpp::NumericVector ex2v(Dim[0]);
    Rcpp::NumericVector count_zero_by_row(Dim[0], N_COL*1.0);

    int col;
    for (int jcol = 0; jcol < N_COL; ++jcol){
      col = colIndex[jcol] - 1; // remove 1 since R indices (input) are 1-based
      for (int j = p[col]; j < p[col + 1]; ++j){
        double diff = x[j] - K;
        exv[i[j]] += diff;
        ex2v[i[j]] += diff*diff;
        count_zero_by_row[i[j]]--;
      }
    }

    // also remove 1 since R indices (input) are 1-based
    exv = exv[H0_genes-1];
    ex2v = ex2v[H0_genes-1];
    count_zero_by_row = count_zero_by_row[H0_genes-1];

    for(int j = 0; j < N_GENES; ++j){
      exv[j] += -1.0*K*count_zero_by_row[j];
      ex2v[j] += K*K*count_zero_by_row[j];
    }
    double ex = ( std::accumulate(exv.begin(), exv.end(), 0.0) ) ;
    double ex2 = ( std::accumulate(ex2v.begin(), ex2v.end(), 0.0) ) ;

    H0_sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );
    H0_sd = H0_sd / sqrt(N_COL*1.0);

    // create a List containing the main results
    Rcpp::List results = Rcpp::List::create(
      Rcpp::Named("lig_avg_expr") = ligands_avg,
      Rcpp::Named("rec_avg_expr") = receptors_avg,
      Rcpp::Named("H0_mean") = H0_mean,
      Rcpp::Named("H0_sd") = H0_sd
    );

    return results;
  } // method BigBootstrap

  Rcpp::List BigBootstrapParallel(Rcpp::NumericVector colIndex,
                                  Rcpp::NumericVector H0_genes,
                                  Rcpp::NumericVector ligands,
                                  Rcpp::NumericVector receptors){

    const int N_COL = colIndex.length();
    const int N_GENES = H0_genes.length();
    const int N_LIG = ligands.length();
    const int N_REC = receptors.length();

    // compute the mean of all genes
    Rcpp::NumericVector geneMeans = rowMeansColIndex(colIndex);

    // compute ligands and receptors means (subset of all genes mean)
    Rcpp::NumericVector ligands_avg = geneMeans[ligands-1];
    Rcpp::NumericVector receptors_avg = geneMeans[receptors-1];

    // compute H0 genes means (subset of all genes mean) to be used in matrix mean computation
    Rcpp::NumericVector H0geneMeans = geneMeans[H0_genes-1];
    // compute matrix mean as the mean of genes means
    double H0_mean = ( std::accumulate(H0geneMeans.begin(), H0geneMeans.end(), 0.0) ) / N_GENES;


    double H0_sd = 0.0;
    double K = H0_mean; // shift parameter
    double n = N_GENES * N_COL; // total number of elements
    Rcpp::NumericVector exv(Dim[0]);
    Rcpp::NumericVector ex2v(Dim[0]);
    Rcpp::NumericVector count_zero_by_row(Dim[0], N_COL*1.0);

    int col;
    for (int jcol = 0; jcol < N_COL; ++jcol){
      col = colIndex[jcol] - 1; // remove 1 since R indices (input) are 1-based
#ifdef _OPENMP
#pragma omp parallel for
#endif
      for (int j = p[col]; j < p[col + 1]; ++j){
        double diff = x[j] - K;
        exv[i[j]] += diff;
        ex2v[i[j]] += diff*diff;
        count_zero_by_row[i[j]]--;
      }
    }

    // also remove 1 since R indices (input) are 1-based
    exv = exv[H0_genes-1];
    ex2v = ex2v[H0_genes-1];
    count_zero_by_row = count_zero_by_row[H0_genes-1];

    for(int j = 0; j < N_GENES; ++j){
      exv[j] += -1.0*K*count_zero_by_row[j];
      ex2v[j] += K*K*count_zero_by_row[j];
    }
    double ex = ( std::accumulate(exv.begin(), exv.end(), 0.0) ) ;
    double ex2 = ( std::accumulate(ex2v.begin(), ex2v.end(), 0.0) ) ;

    H0_sd = sqrt( (ex2 - (ex*ex) / n) / (n - 1.0) );
    H0_sd = H0_sd / sqrt(N_COL*1.0);

    // create a List containing the main results
    Rcpp::List results = Rcpp::List::create(
      Rcpp::Named("lig_avg_expr") = ligands_avg,
      Rcpp::Named("rec_avg_expr") = receptors_avg,
      Rcpp::Named("H0_mean") = H0_mean,
      Rcpp::Named("H0_sd") = H0_sd
    );

    return results;
  } // method BigBootstrapParallel


  std::vector<std::vector<double>> extractCol(Rcpp::NumericVector colIndex) {
    std::vector<std::vector<double>> cols(Dim[0]);
    const int NCOL = colIndex.size();
    int col;
    for (int jcol = 0; jcol < NCOL; ++jcol){
      col = colIndex[jcol] - 1; // remove 1 since R indices (input) are 1-based
      for (int j = p[col]; j < p[col + 1]; ++j){
        cols[i[j]].push_back(x[j]);
      }
    }
    return cols;
  } // method extractCol


  Rcpp::NumericVector extractRow(int row){
    // row = row- 1;

    // Rcpp::NumericVector r(Dim[1], 0.0);
    //
    // auto it = std::find(i.begin(), i.end(), row);
    //
    // for (int col = 0; col < Dim[1]; ++col) {
    //   if(p[col] < std::distance(i.begin(), it)){
    //     continue;
    //   }
    //
    //   for (int j = p[col]; j < p[col + 1]; ++j) {
    //     if (i[j] == row) r[col] = x[j];
    //     else if (i[j] > row) break;
    //   }
    //
    //   it = std::find(it + 1, i.end(), row);
    //   if(it == i.end()){
    //     break;
    //   }
    // }

    Rcpp::NumericVector r(Dim[1], 0.0);



    for (int col = 0; col < Dim[1]; ++col) {
      for (int j = p[col]; j < p[col + 1]; ++j) {
        if (i[j] == row) r[col] = x[j];
        else if (i[j] > row) break;
      }

    }


    // const int NCOL = colIndex.size();
    // int col;
    // for (int jcol = 0; jcol < NCOL; ++jcol){
    //    col = colIndex[jcol]; // remove 1 since R indices (input) are 1-based
    //   for (int j = p[jcol]; j < p[jcol + 1]; ++j){
    //     if (i[j] == row) r[jcol] = x[j];
    //     else if (i[j] > row) break;
    //   }
    // }


    return r;
  }

  template <typename T>
  void fromSparsetoBigMatrix(XPtr<BigMatrix> pMat, MatrixAccessor<T> mat){

    int n = p.size() - 1;
    double it, lo, up;

    for (int j = 0; j < n; j++){
      lo = p[j];
      up = p[j+1];
      for (it = lo; it < up; it++) {
        mat[j][i[it]] = x[it];
      }
    }

  }


};
//}
