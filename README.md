# **scSeqComm**

This repository contains the code of scSeqComm, an R package to analyze intercellular and intracellular signaling from single cell RNA-seq (scRNA-seq) data.

The package can be used to perform

-   **Cellular Communication analysis** within one biological condition (*scSeqComm framework*)
-   **Differential Cellular Communication analysis**, i.e. comparing cell-cell communication between two conditions of interest (*scSeqCommDiff framework*).

To cite this package please use ***"Identify, quantify and characterize cellular communication from single-cell RNA sequencing data with scSeqComm", Giacomo Baruzzo, Giulia Cesaro, Barbara Di Camillo, Bioinformatics, Volume 38, Issue 7, 1 April 2022, Pages 1920--1929***, <https://doi.org/10.1093/bioinformatics/btac036>

## **Updates**

Please install this latest version of scSeqComm

**Novelties**

-   scSeqComm now enables the inferrence of differential cell-cell communication across two experimental conditions via `scSeqComm_differential()` function. Check our tutorial at <https://sysbiobig.gitlab.io/scSeqComm/articles/scseqcomm.html>.

-   Integration of the Omnipath L-R database via `get_omnipath_LR_pairs()`.

-   Implementation of additional intercellular scoring scheme, namely SingleCellSignalR, simplified version of CellChat, product-expression and specificity NATMI weights and scDiffCom scores.

-   Memory-efficient and fast computation through the integration of C++ code to manipulate dgCMatrix or big.matrix object and parallelism through doParallel and doMC packages.

## **News**

-   Check out CClens, an interactive and user-friendly R/Shiny interface to facilitate interpretation and exploration of cell-cell communication data. CClens is available on GitLab at <https://gitlab.com/sysbiobig/cclens>.

-   scSeqComm intercellular score is now integrated in LIANA+ (Dimitrov et al., Nature Cell Biology, 2024) <https://doi.org/10.1038/s41556-024-01469-w>

-   scSeqComm has been identified as one of the top performing cell-cell communication tool in an extensive benchmarking of 18 cell-cell communication inference methods (Lue et al., Genome Research, 2023) <https://doi.org/10.1101/gr.278001.123>  

-   scSeqComm has been selected for oral presentation at ISMB/ECCB 2023 (Youtube recording: <https://www.youtube.com/watch?v=hP-_mtLzMUo>)

-   Advanced visualizations offered by scSeqComm and CClens have been presented as oral presentation at ISMB/ECCB 2023 (Youtube recording: <https://www.youtube.com/watch?app=desktop&v=r5ErgDtbzxg>)

## **Latest applications of scSeqComm in Research**

-   Neurodegenerative Diseases: Le Bars et al. (2024) applied scSeqComm in their study (Molecular Neurobiology) to map cellular communication pathways involved in neurodegenerative disease progression.
-   Human Cellular Pluripotency: Panariello et al. (2023, Nature Communications) used scSeqComm to explore how cell signaling influences pluripotency, offering insights for stem cell research.

## **Installation**

#### Install from GitLab

scSeqComm is available on GitLab at <https://gitlab.com/sysbiobig/scseqcomm>

##### Unix/Linux-based Operating System

To install scSeqComm from GitLab, please use the following commands:

``` r
library(devtools)
install_gitlab("sysbiobig/scseqcomm")
```

The above commands install scSeqComm and the required dependencies. A comprehensive tutorial and all vignettes are available at <https://sysbiobig.gitlab.io/scSeqComm/articles/scseqcomm.html>.


##### Windows-based Operating System

To install scSeqComm from GitLab, please use the following commands:

``` r
library(devtools)
install.packages("doMC", repos="http://R-Forge.R-project.org")
install_gitlab("sysbiobig/scseqcomm")
```

The above commands install scSeqComm and the required dependencies.  A comprehensive tutorial and all vignettes are available at <https://sysbiobig.gitlab.io/scSeqComm/articles/scseqcomm.html>.



## **Getting started**

Please check our tutorial and vignettes <https://sysbiobig.gitlab.io/scSeqComm/articles/scseqcomm.html> 

# 
