# This script is included for reproducibility purposes
# It contains the functions adapted from ToPASeq package version 1.2.0

##########################################################################################
############# BuildGraphNEL script #######################################################
##########################################################################################

#function that builds a graphNEL object from a data.frame describing the edges of this pathway
#MODIFICATION WITH RESPECT TO THE ORIGINAL ToPASeq SCRIPT:
  #modification 1: from line 70 to line 71 to make the function work

buildGraphNEL<-function (edges, sym, edge.types=NULL)
{
  if (!is.null(edge.types))
    edges <- .selectEdges(edges, edge.types)
  if (nrow(edges) == 0)
    g <- new("graphNEL", character(), list(), "directed")
  else {
    prep <- .prepareEdges(edges, sym)
    nodes <- union(unique(prep$src), unique(prep$dest))
    g <- new("graphNEL", nodes, .edLi(nodes, prep), "directed")
    graph::edgeDataDefaults(g, "edgeType") <- "undefined"
    graph::edgeData(g, prep$src, prep$dest, "edgeType") <- prep$type
  }
  return(g)
}

.selectEdges<-function (m, types)
{
  missing <- setdiff(types, edgeAttrs[[1]][,1])
  if (length(missing) > 0) {
    warning("the following edge types will be ommited: ", paste(sort(missing),
                                                                collapse = ", "))
  }
  m[m$type %in% types, , drop = FALSE]
}

.prepareEdges<-function (e, sym)
{
  e[] <- lapply(e, as.character)
  if (sym) {
    e <- .symmetric(e)
  }
  ends <- .endpoints(e)
  types <- tapply(e$type, ends, function(group) {
    paste(sort(unique(group)), collapse = ";")
  })
  binder <- function(...) rbind.data.frame(..., stringsAsFactors = FALSE)
  merged <- do.call(binder, strsplit(names(types), "|", fixed = TRUE))
  colnames(merged) <- c("src", "dest")
  cbind(merged, data.frame(type = as.character(types), stringsAsFactors = FALSE))
}

.symmetric<-function (e)
{
  mask <- e$direction == "undirected" & (e$src_type != e$dest_type | e$src != e$dest)
  dird <- e[!mask, ]
  undir <- e[mask, ]
  revdir <- e[mask, ]
  revdir$src_type <- undir$dest_type
  revdir$src <- undir$dest
  revdir$dest_type <- undir$src_type
  revdir$dest <- undir$src
  rbind(dird, undir, revdir)
}


.endpoints<-function (e)
{
  #paste0( e$src_type,":",e$src,"|", e$dest_type,":", e$dest)
  paste0( e$src,"|", e$dest)
}




.edLi <- function(n, e) {
  sapply(n, function(n) list(edges = e[e[, 1] == n, 2]),
         simplify=FALSE, USE.NAMES = TRUE)
}

#Rgraphviz
.getRenderPar<-function (g, name, what = c("nodes", "edges", "graph"))
{
  what <- match.arg(what)
  nms <- switch(what, nodes = nodes(g), edges = edgeNames(g,
                                                          recipEdges = graphRenderInfo(g, "recipEdges")), graph = "graph")
  ans <- switch(what, nodes = nodeRenderInfo(g, name), edges = edgeRenderInfo(g,
                                                                              name), graph = graphRenderInfo(g, name))
  if (!is.null(ans) && !any(is.na(ans))) {
    if (!is.null(names(ans)))
      ans <- ans[nms]
  }    else {
    default <- parRenderInfo(g, what)[[name]][1]
    if (is.null(default))
      default <- graph.par.get(what)[[name]][1]
    if (is.null(ans)) {
      ans <- rep(default, length(nms))
    }        else {
      if (!is.null(default))
        ans[is.na(ans)] <- default
      ans <- ans[nms]
    }
  }
  ans
}

.renderSpline<-function (spline, arrowhead = FALSE, arrowtail = FALSE, len = 1,
                         col = "black", lwd = 1, lty = "solid", bbox, ...)
{
  mylty <- as.numeric(lty)
  if (!is.na(mylty))
    lty <- mylty
  lapply(spline, lines, col = col, lwd = lwd, lty = lty, ...)
  warn <- FALSE
  xyhead <- tail(bezierPoints(spline[[length(spline)]]), 2)
  if (is.function(arrowhead[[1]])) {
    xy <- list(x = xyhead[2, 1], y = xyhead[2, 2])
    try(arrowhead[[1]](xy, col = col, lwd = lwd, lty = lty))
  }    else {
    warn <- drawHead(arrowhead, xyhead, bbox, col, lwd, lty,
                     len, out = TRUE)
  }
  xytail <- head(bezierPoints(spline[[length(spline)]]), 2)
  if (is.function(arrowtail[[1]])) {
    xy <- list(x = xytail[1, 1], y = xytail[1, 2])
    try(arrowtail[[1]](xy, col = col, lwd = lwd, lty = lty))
  }    else {
    warn <- warn | drawHead(arrowtail, xytail[2:1, ], bbox,
                            col, lwd, lty, len, out = FALSE)
  }
  warn
}



#######################################################################################
############# estimateCF function #####################################################
#######################################################################################
#' Function that takes as input a Pathway object to identify nodes belonging to protein complexes and gene families.
#'
#' @param graph An object of class Pathway
#' @param ligands logical scalar, whether to assign as representive name to each gene family. The representative name is a common part of the names of individual genes. This approach, however, may lead to ambiguities or missings. All the complexes are named by concatenating the names of its components. If it is FALSE, then also gene families are named using the same procedure of complexes.
#'
#' @return List of 2 elements: i) complexes: a list of estimated protein complexes and ii) families: a list of estimated gene families
#'
#' @example #same provided by ToPASeq
#' path <- pathways("hsapiens","kegg")[[1]]
#' estimateCF(path)
#'
#DESCRITPION:
  #Function estimates the multi-subunit protein complexes and gene families in the specific pathway.
  #A protein complex consists of proteins connected by undirected binding interaction.
  #A gene family is a set of nodes with same outgoing and/or incomig edges.

#MODIFICATION WITH RESPECT TO THE ORIGINAL ToPASeq SCRIPT:
  #modification 1: line #178 the graphNEL object, where cliques are identified, is built based on undirected binding edges "e.sub"
                   #insted of all edges of graph "e"
  #modification 2: line #179 is commented since a complex can be composed of 2 units
  #modification 3: line #181 the name assigned to a complex is obtained by concatenating the name of its components
  #modification 4: in the families section, a condition to gene familty definition is added: the set of nodes needs to have same
                   #outgoing and/or incoming edges types.

estimateCF<-function(graph, estimateNames=FALSE){
  if (!any(class(graph)=="Pathway")) stop("graph must be of class 'Pathway',found:", paste(class(graph)))
  e<-graphite::edges(graph,"proteins")
  n<-length(graphite::nodes(graph,"proteins"))
  nod<-graphite::nodes(graph, "proteins")
  nodid<-unname(sapply(nod, function(x) strsplit(x,":")[[1]][2]))

  #COMPLEX:
  #A protein complex consists of proteins connected by undirected binding interaction.

  e.sub<-e[e[,"direction"]=="undirected" & e[,"type"]=="Binding",]
  compNod<-unique(c(e.sub[,"src"],e.sub[,"dest"]))

  if (length(compNod)>0) {
    ff <- graph::subGraph(compNod, buildGraphNEL(e.sub, FALSE, NULL))
    ff <- graph_from_graphnel(ff)
    comp<-gRbase::getCliques(ff)# buildGraphNEL(nod, e, TRUE))) #
    #comp[sapply(comp, length)==2]<-NULL
  } else comp<-NULL
  if (length(comp)>0) names(comp)<-sapply(comp, function(x) paste(x, collapse=",")) #names(comp)<-paste("complex", seq_len(length(comp)),sep="")


  #FAMILY:
  #A gene family is a set of nodes with same outgoing and/or incomig edges.

  outedg <- tapply(e[,"dest"], e[,"src"], function(x) x)[nodid]; names(outedg)<-nodid
  outedg_type <- tapply(e[,"type"], e[,"src"], function(x) x)[nodid]; names(outedg_type)<-nodid
  inedg <- tapply(e[, "src"], e[, "dest"], function(x) x)[nodid]; names(inedg)<-nodid
  inedg_type <- tapply(e[, "type"], e[, "dest"], function(x) x)[nodid]; names(inedg_type)<-nodid

  noout<-sapply(outedg, is.null)
  noin<-sapply(inedg, is.null)

  #outgoing edges
  outMatch<-outer(outedg, outedg, function(x,y) vapply(seq_along(x), function(i) identical(x[[i]],y[[i]]), logical(1)))
  outMatch_type<-outer(outedg_type, outedg_type, function(x,y) vapply(seq_along(x), function(i) identical(x[[i]],y[[i]]), logical(1)))
  outGroups<-apply(outMatch & outMatch_type, 1, function(x) paste(which(x), collapse=","))

  #ingoing edges
  inMatch<-outer(inedg, inedg, function(x,y) vapply(seq_along(x), function(i) identical(x[[i]],y[[i]]), logical(1)))
  inMatch_type<-outer(inedg_type, inedg_type, function(x,y) vapply(seq_along(x), function(i) identical(x[[i]],y[[i]]), logical(1)))
  inGroups<-apply(inMatch & inMatch_type, 1, function(x) paste(which(x), collapse=","))

  io<-cbind(inGroups, outGroups)

  splint<-function(x){
    paste(intersect(strsplit(x,",")[[1]], strsplit(x,",")[[2]]), collapse=",")
  }

  famMid<-unique(apply(io, 1, splint))
  famMid<-lapply(famMid, function(x) nodid[as.numeric(strsplit(x,",")[[1]])])

  fam<-famMid

  single.fam<-sapply(fam, function(x) length(x)==1)
  fam<-fam[!single.fam]


  if (estimateNames) {
    namesF<-sapply(fam, function(X) paste(suppressWarnings(Reduce(f<-function(x,y){x[x %in% y]},strsplit(X,""))), collapse=""))
    w<-which(nchar(namesF)==0)
    if (length(w)>0) warning("Some representative names could not be estimated")
    if (length(namesF) != length(unique(namesF))) warning("Found duplicities in the representative names")
    if (any(namesF %in% nodid)) warning("Found duplicities in the representative names and node names")

    w<-which(nchar(namesF)==0 | namesF %in% nodid | duplicated(namesF) )

    if (length(w)>0) namesF[w]<-sapply(fam[w], function(x) paste(x, collapse=","))
    names(fam)<-namesF
  } else names(fam)<-sapply(fam, function(x) paste(x, collapse=",")) #names(fam)<-paste("family", seq_len(length(fam)), sep="")

  out<-list(complexes=comp, families=fam)
  return(out)
}


##########################################################################################
############# reduceGraph script #######################################################
##########################################################################################
#' Function that takes as input a Pathway object and a list of set of nodes estimated as protein complexes and gene families to simply them into single nodes.
#'
#' @param graph An object of class Pathway
#' @param reduction List of 2 elements: i) complexes: a list of set of nodes estimated as protein complexes and ii) families: a list of set of nodes estimated as gene families
#'
#' @return an object of class Pathway
#'
#' @example #same provided by ToPASeq
#' path <- pathways("hsapiens","kegg")[[1]]
#' red <- estimateCF(path)
#' reduceGraph(path, red)

#DESCRITPION:
#Function simplifies a pathway graph topology. It merges a user specified nodes into a one. The specified set of nodes must be
#either a gene family or a protein complex.



reduceGraph<-function (graph, reduction)
{
  #list of reductions made
  red <- list()

  if (length(reduction$families)>0 | length(reduction$complexes)>0) {
    nod <- graphite::nodes(graph)
    nod <- sapply(nod, function(x) strsplit(x,":")[[1]][2])

    if (!all(unlist(reduction) %in% nod)) stop("Gene identifiers dont match")

    e.old <- graphite::edges(graph)


    ###########################################################
    ########################   COMPLEX   ######################
    ##########################################################

    #estimated protein complexes
    complex <- reduction$complexes

    if (length(complex)>0)
    {
      c <- unlist(complex)

      #recurring nodes: nodes that are contained in more than one set of estimated complexes
      dup <- unique(c[duplicated(c)])

      for (r in 1:length(complex)) {

        newnodes <- c(setdiff(nod, complex[[r]]), names(complex)[r])
        e <- graphite::edges(graph)[graphite::edges(graph)[, "src"] %in% complex[[r]] |
                                      graphite::edges(graph)[, "dest"] %in% complex[[r]], ]

        if (isComplex(graph, complex[[r]])) #check if it is actually a complex
        {
          newedges <- e

          #CASE WHERE THERE IS AT LEAST ONE RECURRING NODE
          #recurring and non-recurring nodes are identified and the adjacent nodes shared by all components of the complex are computed

          if(any(complex[[r]] %in% dup)){

            d <- setdiff(complex[[r]],dup) #non recurring nodes
            curr.dup <- intersect(complex[[r]],dup) #current recurring nodes

            #common adjacent nodes of all units of the complex
            out.c <- Reduce(intersect,tapply(newedges[, "dest"], newedges[, "src"], function(x) x)[complex[[r]]])
            in.c <- Reduce(intersect,tapply(newedges[, "src"], newedges[, "dest"], function(x) x)[complex[[r]]])


            #two cases for a complex fully comprising recurring nodes or not

            if(length(d)>0)
            {
              #AT LEAST ONE NON-RECURRING NODES
              #non-recurring nodes are merged into one node which inherits all their edges. The edges that recurring nodes share with the complex node are removed, as well as the "Binding" connections between recurring and non-recurring nodes.

              #the complex node is composed by all its non recurring nodes and inherits all their edges
              newedges[, "src"][newedges[, "src"] %in% d] <- names(complex)[r]
              newedges[, "dest"][newedges[, "dest"] %in% d] <- names(complex)[r]

              #edges that recurring nodes share with the resulting complex node are deleted
              out.d <- tapply(newedges[, "dest"], newedges[, "src"], function(x) x)[names(complex)[r]][[1]]
              in.d <- tapply(newedges[, "src"], newedges[, "dest"], function(x) x)[names(complex)[r]][[1]]
              newedges <- newedges[!(newedges[,"src"] %in% curr.dup & newedges[,"dest"] %in% setdiff(setdiff(out.d,curr.dup),names(complex[r]))),]
              newedges <- newedges[!(newedges[,"dest"] %in% curr.dup & newedges[,"src"] %in% setdiff(setdiff(in.d,curr.dup),names(complex[r]))),]

              #Binding and Process(binding/association) connections between the complex node and recurring nodes are deleted
              newedges <- newedges[!(newedges[,"src"] %in% curr.dup & newedges[,"dest"] %in% names(complex)[r] & (newedges[,"type"]=="Binding" | newedges[,"type"]=="Process(binding/association)")),]
              newedges <- newedges[!(newedges[,"dest"] %in% curr.dup & newedges[,"src"] %in% names(complex)[r] & (newedges[,"type"]=="Binding" | newedges[,"type"]=="Process(binding/association)")),]
            }
            else{

              #COMPLEX COMPRISED OF ONLY RECURRING NODES
              #the complex node is created with all connections involving adjacent nodes and the same connections are removed by the individual components

              #the complex node inherits all edges connecting the components to the common adjacent nodes
              ed.dup <- newedges[(newedges[,"src"] %in% curr.dup & newedges[,"dest"] %in% out.c) | (newedges[,"dest"] %in% curr.dup & newedges[,"src"] %in% in.c),]
              ed.dup[, "src"][ed.dup[, "src"] %in% curr.dup] <- names(complex)[r]
              ed.dup[, "dest"][ed.dup[, "dest"] %in% curr.dup] <- names(complex)[r]

              newedges <- rbind(newedges,ed.dup)
              rownames(newedges) <- 1:nrow(newedges)
            }

            #CREATING THE PROTEIN COMPLEX NODE
            #information on common adjacent nodes is already taken into account, so it is removed
            newedges <- newedges[!(newedges[,"src"] %in% curr.dup & newedges[,"dest"] %in% out.c),]
            newedges <- newedges[!(newedges[,"dest"] %in% curr.dup & newedges[,"src"] %in% in.c),]
            newedges <- newedges[!duplicated(newedges), ]
            newedges <- newedges[newedges[, "src"] != newedges[, "dest"],
                                 ]
            newedges <- rbind(graphite::edges(graph)[!(graphite::edges(graph)[, "src"] %in%
                                                         complex[[r]] | graphite::edges(graph)[, "dest"] %in% complex[[r]]),
                                                     ], newedges)
            if(nrow(newedges)==0){
              return(NULL)
            }
            rownames(newedges) <- 1:nrow(newedges)
            newedges$src_type <- as.character(newedges$src_type)
            newedges$dest_type <- as.character(newedges$dest_type)
            newedges$direction <- as.character(newedges$direction)
            newedges$type <- as.character(newedges$type)
            graph <- buildPathway(graph@id,graph@title,graph@species,graph@database,newedges)
            red <- c(red,complex[r])

          }

          #CASE WHERE THERE ARE NOT RECURRING NODES: all members are merged into one node, inheriting all their edges.

          else{

            #complex node inherits all edges of its components
            newedges[, "src"][newedges[, "src"] %in% complex[[r]]] <- names(complex)[r]
            newedges[, "dest"][newedges[, "dest"] %in% complex[[r]]] <- names(complex)[r]
            newedges <- newedges[!duplicated(newedges), ]
            newedges <- newedges[newedges[, "src"] != newedges[, "dest"],
                                 ]
            newedges <- rbind(graphite::edges(graph)[!(graphite::edges(graph)[, "src"] %in%
                                                         complex[[r]] | graphite::edges(graph)[, "dest"] %in% complex[[r]]),
                                                     ], newedges)
            if(nrow(newedges)==0)
            {
              #graph with only one node
              return(NULL)
            }
            rownames(newedges) <- 1:nrow(newedges)

            newedges$src_type <- as.character(newedges$src_type)
            newedges$dest_type <- as.character(newedges$dest_type)
            newedges$direction <- as.character(newedges$direction)
            newedges$type <- as.character(newedges$type)
            #re-building the Pathway object with new edges and nodes
            graph <- buildPathway(graph@id,graph@title,graph@species,graph@database,newedges)

            #save information about the newly formed complex
            red <- c(red,complex[r])
          }

        }

      }


      #If in the Pathway there are individual recurring nodes, it means that it is associated to edges in the graph that it is not taken
      #into account in the newly merged nodes. Since it is not possible to associate them to a specific complex or it might happen that
      #they occur as single nodes in the network, those nodes with their remaining edges are connected to the complexes they belong to with
      #"Process(binding/association)"edges
      dup_used <- unique(unlist(red))

      if(length(intersect(dup,dup_used))>0)
      {
        e <- graphite::edges(graph)[graphite::edges(graph)[, "src"] %in% dup |
                                      graphite::edges(graph)[, "dest"] %in% dup, ]
        newedges <- e
        newedges <- newedges[!(e[,"src"] %in% dup & newedges[,"dest"] %in% dup),]

        dup.f <- dup[dup %in% unique(unlist(newedges["src"],newedges["dest"]))]

        if(length(dup.f)>0)
        {
          #these nodes are connected to the complexes in which they are contained with a "Process(binding/association)" edge
          for (j in 1:length(dup.f))
          {
            comp.dup <- red[vapply(seq_along(red), function(i) any((red[[i]] %in% dup.f[j])), logical(1))]
            if(length(comp.dup)==0) next
            ed <- e.old[1:length(comp.dup),]
            ed[,"src"] <- paste(dup.f[j])
            ed[,"src_type"] <- "SYMBOL"
            ed[,"dest_type"] <- "SYMBOL"
            ed[,"dest"]<- names(comp.dup)
            ed[,"direction"] <- "directed"
            ed[,"type"] <- "Process(binding/association)"
            newedges <- rbind(newedges,ed)
            rownames(newedges) <- 1:nrow(newedges)
          }

        }

        newedges <- rbind(graphite::edges(graph)[!(graphite::edges(graph)[, "src"] %in%
                                                     dup | graphite::edges(graph)[, "dest"] %in% dup),
                                                 ], newedges)
        if(nrow(newedges) == 0)
        {
          return(NULL)
        }
        rownames(newedges) <- 1:nrow(newedges)
        newedges$src_type <- as.character(newedges$src_type)
        newedges$dest_type <- as.character(newedges$dest_type)
        newedges$direction <- as.character(newedges$direction)
        newedges$type <- as.character(newedges$type)
        graph <- buildPathway(graph@id,graph@title,graph@species,graph@database,newedges)
      }
    }




    ###########################################################
    ########################   FAMILY   ######################
    ##########################################################

    #estimated gene families
    fam <- reduction$families

    if (length(fam)>0)
    {
      for (r in 1:length(fam)) {
        newnodes <- c(setdiff(nod, fam[[r]]),
                      names(fam)[r])
        e <- graphite::edges(graph)[graphite::edges(graph)[, "src"] %in% fam[[r]] |
                                      graphite::edges(graph)[, "dest"] %in% fam[[r]], ]

        if (nrow(e)==0) next

        #check if the set of nodes is still a gene family (same ingoing and/or outgoing edges)

        outedg <- tapply(e[, "dest"], e[, "src"], function(x) x)[fam[[r]]]
        inedg <- tapply(e[, "src"], e[, "dest"], function(x) x)[fam[[r]]]
        outedg_type <- tapply(as.character(e[, "type"]), e[, "src"], function(x) x)[fam[[r]]]
        inedg_type <- tapply(as.character(e[, "type"]), e[, "dest"], function(x) x)[fam[[r]]]

        outok <- sapply(1:(length(outedg) - 1), function(i) all(outedg[[i]] == outedg[[i + 1]]))
        outok_type <- sapply(1:(length(outedg_type) - 1), function(i) all(outedg_type[[i]] == outedg_type[[i + 1]]))
        inok <- sapply(1:(length(inedg) - 1), function(i) all(inedg[[i]] == inedg[[i + 1]]))
        inok_type <- sapply(1:(length(inedg_type) - 1), function(i) all(inedg_type[[i]] == inedg_type[[i + 1]]))

        if (is.na(all(outok)) | is.na(all(inok))) next

        if (!all(outok) | !all(inok) | !all(outok_type) | !all(inok_type)) next

        #family node inherits all edges of its components
        newedges <- e
        newedges[, "src"][newedges[, "src"] %in% fam[[r]]] <- names(fam)[r]
        newedges[, "dest"][newedges[, "dest"] %in% fam[[r]]] <- names(fam)[r]
        newedges <- newedges[!duplicated(newedges), ]
        newedges <- newedges[newedges[, "src"] != newedges[, "dest"],
                             ]
        newedges <- rbind(graphite::edges(graph)[!(graphite::edges(graph)[, "src"] %in%
                                                     fam[[r]] | graphite::edges(graph)[, "dest"] %in% fam[[r]]),
                                                 ], newedges)
        rownames(newedges) <- 1:nrow(newedges)
        newedges$src_type <- as.character(newedges$src_type)
        newedges$dest_type <- as.character(newedges$dest_type)
        newedges$direction <- as.character(newedges$direction)
        newedges$type <- as.character(newedges$type)
        graph <- buildPathway(graph@id,graph@title,graph@species,graph@database,newedges)
        red <- c(red,fam[r])


        ed <- graphite::edges(graph)

        #CASE: one gene belonging to a family occurs individually and thus it has all family edges and also other edges

        ind1 <- unique(unlist(sapply(1:length(outedg[[1]]),function(g) which(ed[,"dest"] %in% outedg[[1]][g] & ed[,"type"] %in% outedg_type[[1]][g]))))
        ind2 <- unique(unlist(sapply(1:length(inedg[[1]]),function(g) which(ed[,"src"] %in% inedg[[1]][g] & ed[,"type"] %in% inedg_type[[1]][g]))))

        #nodes that share the same edges of the newly formed family
        newcomp <- intersect(ed[ind1,"src"],ed[ind2,"dest"])

        #nodes not indentifies as components of other families or complexes
        adg <- unique(c(ed[ed[,"src"]%in%names(fam)[r],"dest"],ed[ed[,"dest"]%in%names(fam)[r],"src"]))
        newcomp <- newcomp[!(newcomp %in% c(names(red),unique(unlist(fam)),adg))]

        if(length(newcomp)!=0)
        {
          newcomp_final <- as.character()
          for (i in 1:length(newcomp))
          {
            ed.comp <- ed[-c(ind1,ind2),]
            ed.comp <- ed.comp[ed.comp[,"src"] %in% newcomp[i] | ed.comp[,"dest"] %in% newcomp[i],]
            in.c <- sum(ed.comp[,"dest"] %in% newcomp[i])
            out.c <- sum(ed.comp[,"src"] %in% newcomp[i])

            #if the node occurs individually it must have at least one incoming and one outgoing edges not associated to the family

            if(in.c!=0 & out.c!=0)
            {
              out_comm <- nrow(semi_join(ed[ed[,"src"]%in% names(fam)[r],],ed[ed[,"src"]%in% newcomp[i],],by=c("dest","direction","type")))==nrow(ed[ed[,"src"]%in% names(fam)[r],])
              in_comm <- nrow(semi_join(ed[ed[,"dest"]%in% names(fam)[r],],ed[ed[,"dest"]%in% newcomp[i],],by=c("src","direction","type")))== nrow(ed[ed[,"dest"]%in% names(fam)[r],])

              #if the node actually shared the same edges of the family, it is saved in newcomp_final
              if(out_comm & in_comm)
              {
                newcomp_final <- c(newcomp_final,newcomp[i])
              }

            }
          }


          #the nodes identified as components of the family are incorporated in the family node with its family-shared edges
          if(length(newcomp_final)!=0)
          {

            newedges <- graphite::edges(graph)[c(ind1,ind2),]
            newcomp <- newcomp_final
            fam1 <- c(fam[[r]],newcomp)
            name <- paste(fam1, collapse=",")
            red[[length(red)]] <- fam1
            names(red)[length(red)] <- name

            newedges[, "src"][newedges[, "src"] %in% c(names(fam)[r],newcomp)] <- name
            newedges[, "dest"][newedges[, "dest"] %in% c(names(fam)[r],newcomp)] <- name
            newedges <- newedges[!duplicated(newedges), ]
            newedges <- newedges[newedges[, "src"] != newedges[, "dest"],]


            newedges <- rbind(graphite::edges(graph)[-c(ind1,ind2),] ,newedges)
            rownames(newedges) <- 1:nrow(newedges)
            newedges$src_type <- as.character(newedges$src_type)
            newedges$dest_type <- as.character(newedges$dest_type)
            newedges$direction <- as.character(newedges$direction)
            newedges$type <- as.character(newedges$type)
            graph <- buildPathway(graph@id,graph@title,graph@species,graph@database,newedges)

          }
        }#if comp


      }#for fam
    }#if fam


  }
  return(list(graph,red))

}


#internal function that check if a set of nodes x represent a complex
#a protein complex is a set of nodes with only undirected binding edges between them and the number of edges is equal to
#the complex size (a clique).

#modification: If there are some "Process(binding/association)" edges between them, these edges are ignored

isComplex<-function (graph, x) #x is a complex?
{
  if (!any(class(graph)=="Pathway")) stop("graph must be of class 'Pathway',found:", paste(class(graph)))
  e <- graphite::edges(graph,"proteins")
  sube <- e[e[, "src"] %in% x & e[, "dest"] %in% x, ]
  sube <- sube[sube["type"]!= "Process(binding/association)",]
  nrow(sube) == ncol(combn(x,2)) & all(x %in% unlist(sube[, c("src","dest")])) &
    all(sube[, "direction"] == "undirected") &
    all( (regexpr("Binding",sube[, "type"]) != -1) )
}

